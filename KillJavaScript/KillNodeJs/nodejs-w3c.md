##W3C Node.js教程
简单来说node.js就是运行在服务端的javascript
node.js是一个基于chrome javascript运行时建立一个平台
node.js是一个事件驱动i/o服务端javascript环境,基于google的v8引擎,v8引擎执行javascript的速度非常快,性能非常好.
###centOS下安装nodejs
1.下载源码,需要在官网下载最新版本

```
cd /usr/local/src/
wget http://nodejs.org/dist/v0.10.24/node-v0.10.24.tar.gz
```

2.解压源码

```
tar zxvf node-v0.10.24.tar.gz
```

3.编译安装

```
cd node-v0.10.24
./configure --prefix-/usr/local/node/0.10.24
```

4.配置node_home,进入profile编辑环境变量

`vim /etc/profile`

设置nodejs环境变量,在export PATH USER LOGNAME MAIL HOSTNAME HISTSIZE HISTCONTROL一行的上面添加以下内容:

```
#set for nodejs
export NODE_HOME=/usr/local/node/0.10.24
export PATH=$NODE_HOME/bin:$PATH
```

:wq保存并退出,编译/etc/profile使配置生效

`source /etc/profile`

验证是否配置成功,输出版本号表示配置成功

`node -v`

npm模块安装路径

`/usr/local/node/0.10.24/bin/node_modules/`

>注:Nodejs官网提供了编译好的linux二进制包,也可以直接下载下来应用

###Node.js创建http服务器

如果使用php编写后端代码时,需要apache或者nginx的http服务器,并配上mod_php5模块和php-cgi

从这个角度看,整个'接收http请求并提供web页面'的需求根本不需要php处理

不过对Node.js来说,概念完全不一样了.使用Node.js时,我们不仅仅在实现一个应用,同时还实现了整个http服务器,事实上,我们的web应用以及对应的web服务器基本上是一样的

####基础的http服务器

在你的项目根目录创建一个叫server.js的文件,并写入以下代码:

```
var http=require('http');
http.createServer(function(request,response){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end('Hello world\n');
}).linsten(8888);
console.log('Server running at http://127.0.0.1:8888');
```

以上代码我们完成了一个可以工作的http服务器

使用node命令执行以上代码:

```
node server.js
Server running at http://127.0.0.1:8888/
```

接下来,打开浏览器访问 [http://127.0.0.1:8888/](http://127.0.0.1:8888/) 会看到一个写着"Hello world"的网页

分析Node.js的http服务器

- 第一行请求(require)Node.js自带的http模块,并且把它赋值给http变量.
- 接下来我嗯调用http模块提供的函数:createServer.这个函数会返回一个对象,这个对象有一个叫listen的方法,这个方法有一个参数数值,指定这个http服务器监听的端口号

###Node.js模块系统

为了让Node.js的文件可以项目调用,Node.js提供了一个简单的模块系统.

模块是Node.js应用程序的基本组成部分,文件和模块是一一对应的,换言之,一个Node.js文件就是一个模块,这个文件可能是Javascript代码/json或者编译过的c/c++扩展.

####创建模块

在Node.js中,创建一个模块非常简单,如下我们创建一个'main.js'文件,代码如下:

```
var hello=require('./hello');
hello.world();
```

以上实例中,代码require('./hello')引入了当前目录下的hello.js文件(./为当前目录,node.js默认后缀为.js)

Node.js提供了exports和require两个对象,其中exports是模块公开的接口,require用于从外部获取一个模块的接口,即所获取模块的exports对象.

接下来我们就创建hello.js文件,代码如下:

```
exports.world()=functin(){
	console.log('Hello world');
}
```

在以上实例中,hello.js通过exports对象把world作为模块的访问接口,在main.js中通过require('./hello')加载这个模块,然后就可以直接访问main.js中exports对象的成员函数.

有时候我们只是想把一个对象封装到模块中,格式如下:

```
module.exports=function(){
	//...
}
```

例如:

```
//hello.js
function hello(){
	varname;
	this.setName=function(thyName){
		name=thyName;
	};
	this.sayHello=function(){
		console.log('hello '+name);
	};
};
module.exports=hello;
```

这样就可以直接获得这个对象了:

```
//main.js
var hello=require('./hello');
hello=new hello();
hello.setName('BYVoid');
hello.sayHello();
```

模块接口的唯一变化是使用module.exports=hello代替了exports.world=function(){}.在外部引用该模块时,其接口对象就是要输出hello对象本身,而不是原先的exports.

###服务端的模块放在哪里

或许你已经注意到,我们已经在代码中使用了模块,像这样:

```
var http=require('http');
...
http.createServer(...);
```

Node.js中自带了一个叫'http'的模块,我们在我们的代码中请求并把它返回值赋值给了一个本地变量.

这把我们的本地变量变成了一个拥有所有http模块所提供的公共方法的对象.

Node.js的require方法中的文件查找策略如下:

由于Node.js中存在4类模块( **原生模块和3种文件模块** ),尽管require方法极其简单,但是内部的加载却是十分复杂的,其加载优先级也各自不同.

>**优先级:** *1.核心模块>相对路径>文件模块缓存>原生模块>文件加载*  
**模块名:** *2.核心模块/文件模块/目录模块/node_module目录/缓存模块*

#####从文件模块缓存中加载

>尽管原生模块和文件模块优先级不同,但是都不会优先从文件模块的缓存中加载已经存在的模块.

#####从原生模块加载

>原生模块的优先级仅次于文件模块缓存的优先级.require方法在解析文件名之后,优先检查是否在原生模块列表中.以http模块为例,尽管在目录下存在一个http/http.js/http.node/http.js文件,require('http')都不会从这些文件中加载,而是从原生模块中加载.  
>>原生模块也有一个缓存区,同样也是优先从缓存区加载.如果缓存区没有被加载过,则调用原生模块的加载方法进行加载和执行.

#####从文件加载

>当文件模块缓存不存在,而且不是原生模块的时候,Node.js会解析require方法传入的参数,并从文件系统中加载实际的文件.

######require文件接受以下几种参数的传递:

> 
- ***http,fs,path等,原生模块***
- ***./mod或../mod,相对路径的文件夹***
- ***/pathtomodule/mod,绝对路径的文件夹***
- ***mod,非原生模块的文件模块***

###Nodejs事件

Node.js所有的异步i/o操作在完成时都会发送一个事件到事件队列.

Node.js里面的许多对象都会分发事件:一个net.Server对象会在每次有新链接时分发一个事件,一个fs.readStream对象会在文件被打开的时候发出一个事件.所有这些产生事件的对象都是events.EventEmitter的实例.可以通过require('events');来访问该模块.

简单例子来说明EventEmitter的用法:

```
//event.js
var EventEmitter = require('events').EventEmitter;
var event = new EventEmitter();
event.on('some_event',function(){
	console.log('some_event occured.');
});
setTimeout(function(){
	event.emit('some_event');
},1000);
```

运行这段代码,一秒后控制台输出了'some_event occured',原理是event对象注册了事件some_event的一个监听器,然后我们通过setTimeout在1000毫秒以后向event对象发送事件some_event,此时会调用some_event的监听器

###EventEmitter介绍

events模块只提供了一个对象:events.EventEmitter.EventEmitter的核心就是事件发射与事件监听器功能的封装.

EventEmitter的每个事件由一个事件名和若干个参数组成,事件名是一个字符串,通常表达一定的语意.对于每个事件,EventEmitter支持若干个事件监听器.

当事件发射时,注册到这个时间的事件监听器被依次调用,事件参数作为回调函数参数传递.

下面是个例子,解析上面的过程:

```
var events = require('events');
var emitter = new events.EventEmitter();
emitter.on('someEvent',function(arg1,arg2){
	console.log('listener1',arg1,arg2);
});
emitter.on('someEvent',function(arg1,arg2){
	console.log('listener2',arg1,arg2);
});
emitter.emit('someEvent','byvoid',1991);
```

运行的结果是:

```
listener1 byvoid 1991
listener2 byvoid 1991
```

以上例子中,emitter为事件someEvent注册了两个事件监听器,然后发射了someEvent事件,运行结果中可以看到两个事件监听器回调函数被先后调用,这就是EventEmitter最简单的用法.

####EventEmitter常用的API

EventEmitter.on(),emitter.addListener()为指定事件注册了一个监听器,接受一个字符串event和一个回调函数listener.

```
server.on('connection',function(stream){
	console.log('someone connected!');
});
```

EventEmitter.emit(event,[arg1],[arg2][...])发射event事件,传递若干可选参数到事件监听器的参数表.

EventEmitter.once(event,listener)为指定事件注册一个单次监听器,即监听器最多只会触发一次,触发后立刻解除该监听器.

```
server.once('connection',function(stream){
	console.log('Ah,we have our first user!');
});
```

EventEmitter.removeListener(event,listener)移除指定事件的某个监听器,listener必须是该事件已经注册过的监听器.

```
var callback = function(stream){
	console.log('someonce connected!');
};
server.on('connection',callback);
//...
server.removeListener('connection',callback);
```

EventEmitter.removeAllListeners([event])移除所有事件的所有监听器,如果指定event,则移除指定事件的所有监听器.

###error事件

EventEmitter定义了一个特殊的事件error,它包含了'错误'的语义,我们在遇到异常的时候通常会发射error事件.

当error被发射时,EventEmitter规定如果没有响应的监听器,Node.js会把他当作异常,退出程序并打印调用栈.

我们一般要为会发射error事件的对象设置监听器,避免遇到错误后整个程序崩溃,例如:

```
var events = require('events');
var emitter = newevents.EventEmitter();
emitter.emit('error');
```

运行时会显示以下错误:

```
node.js:201
throw e; // process.nextTick error, or 'error' event on first tick
^
Error: Uncaught, unspecified 'error' event.
at EventEmitter.emit (events.js:50:15)
at Object.<anonymous>(/home/byvoid/error.js:5:9)
at Module.load (module.js:348:31)
at Function._load (module.js:308:12)
at Array.0 (module.js:479:10)
at EventEmitter._tickCallback (node.js:192:40)
```

###继承EventEmitter

大多数时候我们不会直接使用EventEmitter,而是在对象中继承它,包括fs,net,http在内的,只要是支持事件响应的核心模块都是EventEmitter的子类.

>为什么要这样做呢,有两点原因:
>  
- 首先,具有某个实体功能的对象实现事件符合语义,事件的监听和发射应该是一个对象的方法.
- 其实javascript的对象机制是基于原型的,支持部分多重继承,继承EventEmitter不会打乱原有的继承关系.

###Node.js函数

在javascript中,一个函数可以作为另一个函数接收的参数.我们可以先定义一个函数,然后传递,也可以在传递参数的地方直接定义函数.

Node.js中函数的使用与javascript类似,举例,可以这样做:

```
function say(word){
	console.log(word);
}
function excute(someFunction,value){
	someFunction(value);
}
execute(say,"Hello");
```

以上代码中,我们把say函数作为execute函数的第一个变量进行了传递.这里返回的不是say的返回值,而是say本身!

这样一来,say就变成了execute中的本地变量someFunction,execute可以通过调用someFunction()(带括号的形式)来使用say函数.

当然,因为say有一个变量,execute在调用someFunction时可以传递这样一个变量.

####匿名函数

我们可以把一个函数作为变量传递.但是我们不一定要绕这个'先定义,再传递'的圈子,我们可以直接在另一个函数的括号中定义和传递这个对象:

```
function execute(someFunction,value){
	someFuncton(value);
}
execute(function(word){console.log(word)},'hello');
```

我们在execute接受第一个参数的地方直接定义了我们准备传递给execute的函数.

用这种方式,我们甚至不用给这个函数取名字,这也为什么他被叫做匿名函数.

###函数传递是如何让http服务器工作的

带着这些知识,我们再来看看我们简约而不简单的http服务器:

```
var http=require('http');
http.createServer(function(request,response){
	response.writeHead(200,{"Content-Type":"text/plain"});
	response.write("hello world");
	response.end();
}).listen(8888);
```

现在他看上去清晰了很多,我们向createServer函数传递了一个匿名函数.

用下面的代码同样达到目的:

```
var http= require('http');
function onRequest(request,response){
	response.writeHead(200,{"Content-Type":"text/plain"});
	response.write("hello world");
	response.end();
};
http.createServer(onRequest).listen(8888);
```

###Node.js路由

我们要为路由提供请求的url和其他需要的get及post参数,随后路由需要根据这些数据来执行相应的代码.

因此,我们需要查看http请求,从中提取出请求的url以及get/post参数,这一功能应当属于路由还是服务器(甚至作为一个模块自身的功能)确实值得探讨,但这里暂定其为我们的http服务器的功能.

我们需要的所有数据都会包含在request对象中,该对象作为onRequest()回调函数的第一个参数传递,但是为了解析这些数据,我们需要额外的Node.js模块,他们分别是url和querystring模块.

```
                   url.parse(string).query
                                           |
           url.parse(string).pathname      |
                       |                   |
                       |                   |
                     ------ -------------------

http://localhost:8888/start?foo=bar&hello=world

                                ---       -----
                                 |          |
                                 |          |
              querystring(string)["foo"]    |
                                            |
                         querystring(string)["hello"]

```

上面表示的是

```
url.parse(string).pathname -> http://localhost:8888/start
url.parse(string).query -> foo=bar&hello=world
querystring(string)["foo"] -> bar
querystring(string)["hello"] -> world
```

当然我们也可以使用querystring模块来解析post请求体重的参数,下面会有演示.

现在我们来给onRequest()函数加上一些逻辑,用来找出浏览器请求的url路径:

```
var http = require('http');
var url = require('url');
function start(){
	function onRequest(request,response){
		var pathname=url.parse(request.url).pathname;
		console.log("Request for"+pathname+"received.");
		response.writeHead(200,{"Content-Type":"text/plain"});
		response.write("Hello world");
		response.end();
	}
http.createServer(onRequest).listen(8888);
console.log("Server has started.");
}
exports.start=start;
```

好了,我们的应用新建在可以通过请求的url路径来区别不同的请求了,这使我们得以使用路由(还未完成)来将请求以url路径为基准映射到处理程序上.

在我们所要构建的应用中,这意味着来自/start和/upload的请求可以使用不同的代码来处理,稍后我们将看到这些内容是如何整合到一起的.

现在可以编写路由,建立一个名为router.js文件,添加内容如下:

```
function route(pathname){
	console.log("About to route a request for "+pathname);
}
exports.route=route;
```

如你所见,这段代码什么都没干,不过对于现在来说这是应该的,在添加更多的逻辑以前,我们先来看看如何把路由和服务器整合起来.

我们的服务器应当知道路由的存在并加以利用.我们当然可以筒骨弓硬编码的方式将这一依赖项绑定到服务器上,但是其他语言的编程经验告诉我们这会是一件非常痛苦的事情,因此我们将使用依赖注入的方式轻松散的添加路由模块.

首先,我们来扩展下服务器的start()函数,以便将路由函数作为参数传递过去:

```
var http = require('http');
var url = require('url');
function start(route){
	function onRequest(){
		var pathnme = url.parse(request.url).pathname;
		console.log("Request for "+pathname+" received.");
		route(pathname);
		response.writeHead(200,{"Content-Type":"text/plain"});
		response.write("hello world");
		response.end();
	}
http.createServer(onRequest).listen(8888);
console.log("Server has started.");
}
exports.start=start;
```

区别就是start方法多了route参数,start方法内多了`route(pathname);`.

同级目录下编写index.js,使得路由函数可以被注入到服务器中:

```
var server = require('./server');
var router = require('./router');
server.start(router.route);
```

在这里我们传递的参数依旧什么都没做

如果现在启动应用(`node index.js`,始终记得这个命令行),随后请求一个url,你将会看到应用输出相应的信息,这表明我们的服务器已经在使用路由模块了,并会将请求的路径传递给路由:

```
base$ node index.js
Request for /foo received.
About to route a request for /foo
```

以上输出已经去掉了比较烦人的/favicon.ico请求相关的部分.

>
命令行执行`node index.js`,会输出  
`Server has started.`  
在浏览器中访问`localhost:8888`,命令行会输出  
`Request for / received.  `  
`About to route a request for /`  
`Request for /favicon.ico received.  `  
`About to route a request for /favicon.ico`  

###Nodejs全局对象

javascript中有一个特殊的对象,称为全局对象(global object),它及其所有属性都可以在程序的任何地方访问,即全局变量.

在浏览器javascript中,重唱window是全局对象,而Nodejs中的全局对象是global,所有的全局变量(除了global本身以外)都是global对象的属性

我们在nodejs中能够直接访问到的对象通常都是global的属性,如console,process等,下面逐一介绍.

####全局对象和全局变量

global最根本的作用是最为全局变量的宿主.按照ECMAScript的定义,满足以下条件的变量是全局变量:

- 在最外层定义的变量
- 全局对象的属性
- 隐式定义的变量(未定义直接赋值的变量)

当你定义一个全局变量时,这个变量同时也会成为全局对象的属性,反之亦然.需要注意的是,在node.js中你不可能在最外层定义变量,因为所有用户都是属于当前模块的,而模块本身不是最外层上下文.

>**注意:**永远使用var定义变量以避免引入全局变量,因为全局变量会污染命名空间,提高代码耦合风险.

####process

process是一个全局变量,即global对象的属性

它用于描述当前node.js进程状态的对象,提供了一个与操作系统的简单接口,通常在你写本地命令行程序的时候,少不了要和他打交道,下面将会介绍process对象的一些最常用的成员方法.

process.argv是命令行参数数组,第一个元素是node,第二个元素是脚本文件名,从第三个元素开始每个元素是一个运行参数.

`console.log(process.argv);`

将以上代码存储为argv.js,通过以下命令运行:

```
$ node argv.js 1991 name-byvoid --v "Carbo Kuo"
['node',
'/home/byvoid/argv.js',
'1991',
'name=byvoid',
'--v',
'Carbo Kuo']
```

- **process.stdout**是标准输出流,通常我们使用的console.log()向标准输出打印字符,而process.stdout.write()函数提供了更底层的接口.
- **process.stdin**是标准输入流,初始时他是被暂停的,要想从标准输入读取数据,你必须恢复流,并手动编写流的事件响应函数.

```
process.stdin.resuem();
process.stdin.on('data',function(data){
	process.stdout.write('read from console :'+data.toString());
});
```

>执行 node process.js没反应..不知道是不是新版本没有这个特性了

-- **process.nextTick(callback)**的功能是为事件循环设置一项任务,Nodejs会在下次事件循环调响应时调用callback.

初学者很可能不理解这个函数的作用,有什么任务不能在当下执行完,需要交给下次事件循环响应来做呢?

我们讨论过,Node.js适合i/o密集型的应用,而不是计算密集型的应用,因为一个Node.js进行只有一个线程,因此在任何时刻都只有一个事件在执行.

如果这个事件占用大量的cpu时间,执行事件循环中的下一个事件就需要等待很久,因此Node.js的一个编程原则就是尽量缩短每个事件的执行时间.process.nextTick()提供了一个这样的工具,可以把复杂的工作拆散,变成一个个较小的事件.

```
function doSomething(args, callback) { 
	somethingComplicated(args); 
	callback(); 
} 
doSomething(function onEnd() { 
	compute(); 
}); 
```

我们假设compute() 和somethingComplicated() 是两个较为耗时的函数，以上 的程序在调用 doSomething() 时会先执行somethingComplicated()，然后立即调用 回调函数，在 onEnd() 中又会执行 compute()。下面用process.nextTick() 改写上 面的程序： 

```
function doSomething(args, callback) { 
	somethingComplicated(args); 
	process.nextTick(callback); 
} 
doSomething(function onEnd() { 
	compute(); 
}); 
```

改写后的程序会把上面耗时的操作拆分为两个事件，减少每个事件的执行时间，提高事 件响应速度。 

>
**注意:** 不要使用setTimeout(fn,0)代替process.nextTick(callback)， 前者比后者效率要低得多。

我们探讨了process对象常用的几个成员，除此之外process还展示了process.platform、 process.pid、process.execPath、process.memoryUsage() 等方法，以及POSIX 进程信号响应机制。有兴趣的读者可以访问[http://nodejs.org/api/process.html](http://nodejs.org/api/process.html) 了解详细 内容。

###console

console 用于提供控制台标准输出，它是由Internet Explorer 的JScript 引擎提供的调试 工具，后来逐渐成为浏览器的事实标准。

Node.js 沿用了这个标准，提供与习惯行为一致的 console 对象，用于向标准输出流（stdout）或标准错误流（stderr）输出字符。 ? console.log()：向标准输出流打印字符并以换行符结束。

console.log 接受若干 个参数，如果只有一个参数，则输出这个参数的字符串形式。如果有多个参数，则 以类似于C 语言 printf() 命令的格式输出。

第一个参数是一个字符串，如果没有 参数，只打印一个换行。

```
console.log('Hello world'); 
console.log('byvoid%diovyb'); 
console.log('byvoid%diovyb', 1991); 
```

运行结果为： 

```
Hello world 
byvoid%diovyb 
byvoid1991iovyb 
```

>
然而我自己的结果并不是那样,百分号输出还是百分号,参数没有被替换掉  
后来发现是因为必须要有`%d`才行....,聪明的我啊,还以为语法不一样一个`%`就行呢

- console.error()：与console.log() 用法相同，只是向标准错误流输出。 
- console.trace()：向标准错误流输出当前的调用栈。 

`console.trace();`


运行结果为：

```
Trace: 
at Object.<anonymous> (/home/byvoid/consoletrace.js:1:71) 
at Module._compile (module.js:441:26) 
at Object..js (module.js:459:10) 
at Module.load (module.js:348:31) 
at Function._load (module.js:308:12) 
at Array.0 (module.js:479:10) 
at EventEmitter._tickCallback (node.js:192:40)
```

###NodeJS常用工具 Util

util 是一个Node.js 核心模块，提供常用函数的集合，用于弥补核心JavaScript 的功能 过于精简的不足。 

---

####util.inherits

`util.inherits(constructor, superConstructor)`是一个实现对象间原型继承 的函数。 

JavaScript 的面向对象特性是基于原型的，与常见的基于类的不同。JavaScript 没有 提供对象继承的语言级别特性，而是通过原型复制来实现的。

在这里我们只介绍util.inherits 的用法，示例如下： 

```
var util = require('util'); 
function Base() { 
	this.name = 'base'; 
	this.base = 1991; 
	this.sayHello = function() { 
		console.log('Hello ' + this.name); 
	}; 
} 
Base.prototype.showName = function() { 
	console.log(this.name);
}; 
function Sub() { 
	this.name = 'sub'; 
} 
util.inherits(Sub, Base); 
var objBase = new Base(); 
objBase.showName(); 
objBase.sayHello(); 
console.log(objBase); 
var objSub = newSub(); 
objSub.showName(); 
//objSub.sayHello(); 
console.log(objSub); 
```

我们定义了一个基础对象Base 和一个继承自Base 的Sub，Base 有三个在构造函数 内定义的属性和一个原型中定义的函数，通过util.inherits 实现继承。运行结果如下：

```
base 
Hello base 
{ name: 'base', base: 1991, sayHello: [Function] } 
sub 
{ name: 'sub' }
```

>
**注意：**Sub 仅仅继承了Base 在原型中定义的函数，而构造函数内部创造的 base 属 性和 sayHello 函数都没有被 Sub 继承。  
Base.prototype中的prototype属性指定在Base的原型中定义

同时，在原型中定义的属性不会被console.log 作 为对象的属性输出。如果我们去掉 objSub.sayHello(); 这行的注释，将会看到：

```
node.js:201 
throw e; // process.nextTick error, or 'error' event on first tick 
^ 
TypeError: Object #<Sub> has no method 'sayHello' 
at Object.<anonymous> (/home/byvoid/utilinherits.js:29:8) 
at Module._compile (module.js:441:26) 
at Object..js (module.js:459:10) 
at Module.load (module.js:348:31) 
at Function._load (module.js:308:12) 
at Array.0 (module.js:479:10) 
at EventEmitter._tickCallback (node.js:192:40) 
```


---

####util.inspect

`util.inspect(object,[showHidden],[depth],[colors])`是一个将任意对象转换为字符串的方法，通常用于调试和错误输出。它至少接受一个参数 object，即要转换的对象。 

showHidden 是一个可选参数，如果值为 true，将会输出更多隐藏信息。

depth 表示最大递归的层数，如果对象很复杂，你可以指定层数以控制输出信息的多少。如果不指定depth，默认会递归2层，指定为 null 表示将不限递归层数完整遍历对象。 如果color 值为 true,输出格式将会以ANSI颜色编码，通常用于在终端显示更漂亮的效果。

特别要指出的是，util.inspect 并不会简单地直接把对象转换为字符串，即使该对 象定义了toString 方法也不会调用。

```
var util = require('util'); 
function Person() { 
	this.name = 'byvoid'; 
	this.toString = function() { 
		return this.name; 
	}; 
} 
var obj = new Person(); 
console.log(util.inspect(obj)); 
console.log(util.inspect(obj, true)); 
```

运行结果是： 

```
{ name: 'byvoid', toString: [Function] } 
{ toString: 
{ [Function] 
[prototype]: { [constructor]: [Circular] }, 
*[caller]: null, 
*[length]: 0,
*[name]: '', 
[arguments]: null }, 
name: 'byvoid' } 
```

>把那三行的 * 去掉,可能是被当作超链接隐藏了,使用 \ 转义也不行,出此下策.

--

####util.isArray(object)
如果给定的参数 "object" 是一个数组返回true，否则返回false。

```
var util = require('util');

util.isArray([])
  // true
util.isArray(new Array)
  // true
util.isArray({})
  // false
```

--

####util.isRegExp(object)

如果给定的参数 "object" 是一个正则表达式返回true，否则返回false。

```
var util = require('util');

util.isRegExp(/some regexp/)
  // true
util.isRegExp(new RegExp('another regexp'))
  // true
util.isRegExp({})
  // false
```

--

####util.isDate(object)

如果给定的参数 "object" 是一个日期返回true，否则返回false。

```
var util = require('util');

util.isDate(new Date())
  // true
util.isDate(Date())
  // false (without 'new' returns a String)
util.isDate({})
  // false
```

--

####util.isError(object)
如果给定的参数 "object" 是一个错误对象返回true，否则返回false。

```
var util = require('util');

util.isError(new Error())
  // true
util.isError(new TypeError())
  // true
util.isError({ name: 'Error', message: 'an error occurred' })
  // false
```

更多详情可以访问 [http://nodejs.org/api/util.html](http://nodejs.org/api/util.html) 了解详细内容。 

###Node.js 文件系统

Node.js 文件系统封装在 fs 模块是中，它提供了文件的读取、写入、更名、删除、遍历目录、链接等POSIX 文件系统操作。

与其他模块不同的是，fs 模块中所有的操作都提供了异步的和 同步的两个版本，例如读取文件内容的函数有异步的 fs.readFile() 和同步的 fs.readFileSync()。我们以几个函数为代表，介绍 fs 常用的功能，并列出 fs 所有函数 的定义和功能。 


---

####fs.readFile
Node.js读取文件函数语法如下： 

`fs.readFile(filename,[encoding],[callback(err,data)])`

- filename（必选），表示要读取的文件名。 
- encoding（可选），表示文件的字符编码。 
- callback 是回调函数，用于接收文件的内容。

如果不指 定 encoding，则 callback 就是第二个参数。回调函数提供两个参数 err 和 data，err 表 示有没有错误发生，data 是文件内容。如果指定了 encoding，data 是一个解析后的字符串，否则 data 将会是以 Buffer 形式表示的二进制数据。

例如以下程序，我们从content.txt 中读取数据，但不指定编码：

```
varfs = require('fs'); 
fs.readFile('content.txt', function(err, data) { 
	if(err) { 
		console.error(err); 
	} else{ 
		console.log(data); 
	} 
}); 
```

假设content.txt中的内容是UTF-8编码的`Text 文本文件示例`，运行结果如下：

`<Buffer 54 65 78 74 20 e6 96 87 e6 9c ac e6 96 87 e4 bb b6 e7 a4 ba e4 be 8b> `

这个程序以二进制的模式读取了文件的内容，data 的值是 Buffer 对象。如果我们给 

fs.readFile 的 encoding 指定编码： 

```
var fs = require('fs'); 
	fs.readFile('content.txt', 'utf-8', function(err, data) { 
	if (err) { 
		console.error(err); 
	} else { 
		console.log(data); 
	} 
}); 
```

那么运行结果则是： 

`Text 文本文件示例`

当读取文件出现错误时，err 将会是 Error 对象。如果content.txt 不存在，运行前面 的代码则会出现以下结果：

```
{ [Error: ENOENT, no such file or directory 'content.txt']
errno: 34, code:'ENOENT', 
path: 'content.txt' } 
```

--

####fs.readFileSync

fs.readFileSync(filename, [encoding])是 fs.readFile同步的版本。它接受的参数和fs.readFile 相同，而读取到的文件内容会以函数返回值的形式返回。如果有错误发生，fs 将会抛出异常，你需要使用 try 和 catch 捕捉并处理异常。 

>
**注意：**与同步I/O函数不同，Node.js 中异步函数大多没有返回值。  
fs.readFileSync()是同步函数且有返回值.

---

####fs.open
`fs.open(path, flags, [mode], [callback(err, fd)])`是POSIX open函数的封装，与C语言标准库中的fopen函数类似。它接受两个必选参数，path 为文件的路径， flags 可以是以下值。

- r ：以读取模式打开文件。 
- r+ ：以读写模式打开文件。 
- w ：以写入模式打开文件，如果文件不存在则创建。 
- w+ ：以读写模式打开文件，如果文件不存在则创建。 
- a ：以追加模式打开文件，如果文件不存在则创建。 
- a+ ：以读取追加模式打开文件，如果文件不存在则创建 

--

####fs.read

fs.read语法格式如下：

`fs.read(fd, buffer, offset, length, position, [callback(err, bytesRead, buffer)])`

参数说明：

- fd: 读取数据并写入 buffer 指向的缓冲区对象。 
- offset: 是buffer 的写入偏移量。 
- length: 是要从文件中读取的字节数。 
- position: 是文件读取的起始位置，如果 - position 的值为 null，则会从当前文件指针的位置读取。 
- callback:回调函数传递bytesRead 和 buffer，分别表示读取的字节数和缓冲区对象。 

以下是一个使用 fs.open 和 fs.read 的示例。 

```
varfs = require('fs'); 
fs.open('content.txt', 'r', function(err, fd) { 
	if(err) { 
		console.error(err); 
		return; 
	} 
	var buf = newBuffer(8); 
	fs.read(fd, buf, 0, 8, null, function(err, bytesRead, buffer) { 
		if(err) { 
			console.error(err); 
			return; 
		} 
		console.log('bytesRead: ' + bytesRead); 
		console.log(buffer); 
	}) 
}); 
```

运行结果是：

```
bytesRead: 8 
<Buffer 54 65 78 74 20 e6 96 87> 
```

一般来说，除非必要，否则不要使用这种方式读取文件，因为它要求你手动管理缓冲区 和文件指针，尤其是在你不知道文件大小的时候，这将会是一件很麻烦的事情。 

####fs模块函数表

可点击查看：[http://nodejs.org/api/fs.html](http://nodejs.org/api/fs.html)

###Node.js GET/POST请求 

在很多场景中，我们的服务器都需要跟用户的浏览器打交道，如表单提交。

表单提交到服务器一般都使用GET/POST请求。

本章节我们将为大家介绍 Node.js GET/POST请求。

---

####获取GET请求内容

由于GET请求直接被嵌入在路径中，URL是完整的请求路径，包括了?后面的部分，因此你可以手动解析后面的内容作为GET请求的参数。 

node.js中url模块中的parse函数提供了这个功能。

```
var http = require('http');
var url = require('url');
var util = require('util');

http.createServer(function(req, res){
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end(util.inspect(url.parse(req.url, true)));
}).listen(3000);
```

在浏览器中访问[http://localhost:3000/user?name=w3c&email=w3c@w3cschool.cc](http://localhost:3000/user?name=w3c&email=w3c@w3cschool.cc) 然后查看返回结果.

####获取POST请求内容

POST请求的内容全部的都在请求体中，http.ServerRequest并没有一个属性内容为请求体，原因是等待请求体传输可能是一件耗时的工作。

比如上传文件，而很多时候我们可能并不需要理会请求体的内容，恶意的POST请求会大大消耗服务器的资源，所有node.js默认是不会解析请求体的， 当你需要的时候，需要手动来做。 

```
var http = require('http');
var querystring = require('querystring');
var util = require('util');

http.createServer(function(req, res){
	//定义了一个post变量，用于暂存请求体的信息
    var post = '';     

    req.on('data', function(chunk){ 
		//通过req的data事件监听函数，每当接受到请求体的数据，就累加到post变量中   
        post += chunk;
    });

    req.on('end', function(){
		//在end事件触发后，通过querystring.parse将post解析为真正的POST请求格式
		//然后向客户端返回。    
        post = querystring.parse(post);
        res.end(util.inspect(post));
    });
}).listen(3000);
```

>**参考:**  
**util的inspect方法将任意对象转换为字符串,通常用于调试和错误输出。它至少接受一个参数 object，即要转换的对象。**

>>**路由:**

>>
```
url.parse(string).pathname -> http://localhost:8888/start
url.parse(string).query -> foo=bar&hello=world
querystring(string)["foo"] -> bar  
querystring(string)["hello"] -> world  
```
>>

