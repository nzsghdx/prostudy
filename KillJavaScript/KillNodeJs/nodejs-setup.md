##nodejs学习
由于node.js平台是后端运行javascript代码,所以必须现在本地安装node环境
###安装node.js
####下载
- [nodejs官网下载](www.example.com)
- [国内镜像](www.example.com)

在windows上安装务必选择全部组件

![setup](./image/nodejs-setup.png)

安装完成之后,在windows环境下的cmd下,输入`node -v`

![command](./image/command.png)

如果正常安装会看到输入`v0.12.7`这样的版本号

```
$ node -v
v0.12.7
```
继续在cmd下输入`node`,进入到node.js的交互环境,在交互环境下,可以输入任何javascript语句,回车得到结果:

![command-node](./image/command-node.png)

要退出node.js环境,请连续两次ctrl+c
###npm
在正式开始学习node.js之前,需要先认识一下npm
npm其实是node.js的包管理工具(nodejs package manager)
######包管理工具的作用
>在node.js上开发时,会用到很多别人写的javascript代码,如果要使用别人写的某个包,每次都根据名称搜索一下官方网站,下载代码,解压,再使用,非常繁琐,于是一个集中管理的工具应运而生,大家都把自己开发的模块打包后放到nmp的官网上,如果使用,直接通过npm安装就可以直接用,不用管代码在哪,应该从哪下载等.

>更重要的是,如果我们要使用模块a,而模块a又依赖于模块b,模块b又依赖于模块x和模块y,npm可以根据依赖关系,把所有的依赖的包都下载并管理起来,否则靠我们自己手动管理,肯定麻烦又容易出错.

npm已经在node.js安装的时候就顺带安装好了,我们在命令提示符或者终端输入`npm -v`,就能看到以下类似输出:

```
C:\>npm -v
2.11.3
```

如果直接输入`npm`,会看到以下类似输出

```
C:\>npm
Usage: npm <command>
where <command> is one of:
	...
```
意思就是`npm`需要跟上命令.

























