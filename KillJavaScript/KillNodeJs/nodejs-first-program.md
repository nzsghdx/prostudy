##第一个node程序
以前我们编写的javascript代码都是在浏览器中运行的,因此我们可以直接在浏览器中敲代码,然后直接运行
有了nodejs之后,可以在node环境中执行,javascript代码将直接在计算机上以命令行的方式运行,你需要一个顺手的文本编辑器来编写javascriptdiamagnetic,并且把它保存到本地的某个目录下,才能执行
在文本编辑器中输入以下代码:

```
'use strict';
console.log('hello,world');
```

第一行总写上`use strict;`是因为使用严格模式运行javascript代码可以避免各种潜在陷阱
然后选择一个本地目录,把文件保存,然后就可以打开命令行窗口,把当前目录切换到文本保存位置的目录下,然后输入以下命令就可以运行这个程序了:

```
C:\Workspacd>node hello.js
hello,world.
```

>文本名只能是英文字母/数字/下划线的组合
如果当前目录下没找到你的文本文件,就会报错:

```
C:\Workspacd>node hello.js
module.js:338
	throw err:

Error:Cannot find module 'C:\Workspacd>node hello.js'
	at Function.Module._resolveFilename
	at Function.Module._load
	at Function.Module.runMain
	at startup
	at node.js
```

###命令行和mode交互模式
请注意区分命令行模式和node交互模式
看到类似`C:\>`是在windows提供的命令行模式:

![command-windows](./image/nodejs-windows-command.png)

在命令行模式下,可以执行`node`进入node交互环境,也可以执行`node hello.js`运行要给`.js`文件
看到`>`是在node交互式环境下:

![command-node](./image/nodejs-node-command.png)

在node交互环境下,可以输入javascript代码立刻执行
此外,在命令行模式下运行`.js`文件和在node交互环境下直接运行javascript代码有所不同,node交互式环境会把每一行javascript代码的结果自动打印出来,但是直接运行javascript文件却不会.
例如,在node交互环境下:

```
>100 +200 +300
600
```

直接可以看到结果`600`
但是如果是`calc.js`文件内容如下

`100+200+300`

然后windows在命令行模式下执行:

```
C:\Workspace>node calc.js
```

发现没有任何输出.
这是正常的,想要输出结果必须自己用`console.log()`打印出来,改造如下:

```
console.log(100+200+300);
```

再执行就可以看到结果:

```
C:\Workspace>node calc.js
600
```

###node交互环境和windows的区别
>直接输入node进入交互环境,相当于启动了node解释器,等待你一行一行的输入源代码,每输入一行就执行一行
直接运行`node hello.js`文件相当于启动了node解释器,然后一次性的把`hello.js`文件的源代码给执行了,没有机会以交互的方式输入源代码的
























