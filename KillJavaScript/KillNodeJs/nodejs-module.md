##node中的模块(module)
为了编写可维护的代码,会把很多函数分组,分别放到不同的文件里,这样每个文件包含的代码就相对较少,很多编程语言都采用这种组织代码的方式.在node环境中,一个.js文件就称之为一个模块(module).
####使用模块的好处

>大大提高了代码的可维护性,编写代码不必从零开始.当一个模块编写完毕,就可以被其他地方引用.我们在自己编写模块时,不必考虑名字会与其他模块冲突

例如`hello.js`就是一个模块,模块的名字就是文件名(去掉`.js`后缀),所以`hello.js`文件就是名为`hello`的模块
改造`hello.js`,创建一个函数,可以在其他地方调用这个函数:

```
'use strict';
var s = 'Hello';
function greet(name){
	console.log(s+","+name+'!');
}
module.export=greet;
```

函数`greet`是在`hello`模块中定义的,最后一行是一个奇怪的赋值语句,意思是把函数`greet`作为模块的输出暴露出去,这样其他模块就可以使用`greet`函数了
那么其他模块怎么使用`hello`模块的`greet`函数呢,需要编写另外一个模块`main.js`,调用`hello`模块的`greet`函数:

```
'use strict';
//引入hello模块
var greet =require('./hello');
var s = "Michael";
greet(s);//hello,Michael!
```

注意引入`hello`模块用的是node提供的`require`函数:

`var greet =require('./hello');`

引入的模块作为变量保存在`greet`变量中,那`greet`变量到底是什么东西呢?其实变量`greet`就是在`hello.js`中用了`module.exports=greet;`输出的`greet`函数,所以`main.js`就成功的引入了`hello.js`模块中定义的`greet`函数,接下来就可以直接使用它了.
在使用`require()`引入模块的使用,请注意模块的相对路径,因为`main.js`和`hello.js`位于同一个目录,所以使用当前目录`.`:

`var greet = require('./hello');//相对目录`

如果只写模块名:

`var greet = require('hello');`

则node会依次在内置函数,全局函数和当前模块下查找`hello.js`,可能会得到一个错误:

```
module.js
	throw err:
Error: Cannot find module 'hello'
	at Function.Module._resolveFilename
	at Function.Module._load
	...
	at Function.Module._load
	at Function.Moduel.runMain
```

遇到这个错误,需要检查:

- 模块名是否写对了
- 模块文件是否存在
- 相对路径是否写对了

###CommonJS规范
这种模块加载机制被称为是CommonJS规范.在这个规范下每个`.js`文件都是一个模块,他们内部各自使用的变量名和函数名称都互不冲突,例如`hello.js`和`main.js`都申明了全局变量`var s ='xxx'`,但互不影响.

>一个模块想要对外暴露变量(函数也是变量),可以用`module.exports=variable;`,一个模块要引用其他变量用`var ref=require('module_name');`就拿到了引用模块的变量.

###结论

要在模块中对外输出变量用:

`module.exports=variable;`

输出的变量可以是任意对象,函数,数组等等
要引入其他模块输出的对象,用:

`var foo =require('other_module');`

引入的对象具体是什么,取决于引入模块输出的对象

###深入了解模块原理

如果你想详细的了解CommonJS的模块实现原理,请继续往下阅读.

当我们编写javascript代码时,我们可以声明全局变量:

`var s = 'global';`

在浏览器中,大量的使用全局变量不是个明智的选择.如果在`a.js`和`b.js`中都使用了全局变量`s`,将会造成冲突,`b.js`中对`s`的复制会改变`a.js`的运行逻辑.
javascript语言本身并没有一种模块机制来保证不同模块可以使用相同的变量名.
######nodejs是怎么实现这一点的呢?
实现'模块'这个功能并不需要语法层面的支持.node.js也并不会增加任何javascript语法,实现'模块'功能的奥妙在于javascript是一种函数式编程语言,它支持闭包.如果把一段javascript代码用一个函数包装起来,这段代码就把所有的'全局变量'就变成了函数内部的局部变量.
注意上面编写的`hello.js`代码是这样写的:

```
var s = "hello";
var name = "world";
console.log(s+","+name+"!");
```

node.js加载了`hello.js`后,他可以把代码包装一下,变成以下这种格式:

```
(function(){
	//读取hello.js代码
	var s = "hello";
	var name = "world";
	console.log(s+","+name+"!");
	//hello.js代码结束
})
```

这样一来,原来的全局变量`s`现在就变成了函数内部的局部变量.如果node.js继续加载其他模块,这些模块中定义的全局变量`s`也互不干扰.

所以node利用javascript的函数式编程的特性,轻而易举的实现了模块的隔离.

但是模块的输出`module.exports`是怎么实现的?
实现如下:
node可以先准备一个对象`module`:

```
//准备module对象:
var module={
	id:"hello",
	exports: {}
};
var load = function(){
	//读取hello.js代码
	function greet(name){
		console.log("hello,"+name+"!");
	}
	module.exports=greet;
	//hello.js代码结束
	return module.exports;
};
var exported = load(module);
//保存module
save(module,exported);
```

可见,变量`module`是node在加载js文件前准备的一个变量,并将其传入加载函数,我们在`hello.js`中可以直接使用变量`module`的原因就在于它实际上是函数的一个参数:

`module.exports=greet;`

通过把参数`module`传递给`load()`函数,`hello.js`就顺利的把一个变量传递给了node执行环境,node会把`module`变量保存到某个地方.

由于node保存了所有导入的`module`,当我们把`require()`获取module时,node找到了对应的`module`,把这个`module`的`exports`变量返回,这样,另一个模块就顺利的拿到了模块的输出:

`var greet = require('./hello');`

以上是node实现javascript模块的一个简单的原理介绍.

###module.exports vs exports

在node环境中,有两种方法可以在一个模块中输出变量:

方法一:对module.exports赋值:

```
//hello.js
function hello(){
	console.log("hello.world!");
}
function greet(){
	console.log("hello,"+name+"!"):
}
function hello(){
	console.log("hello,world!");
}
module.exports={
	hello:hello,
	greet:greet
}
```

方法二:直接使用exports:

```
//hello.js
function hello(){
	console.log("hello.world!");
}
function greet(){
	console.log("hello,"+name+"!"):
}
function hello(){
	console.log("hello,world!");
}
exports.hello=hello;
exports.greet=greet;
```

但是不能直接对`exports`赋值:

```
//代码可以执行,但是模块并没有输出任何变量:
exports={
	hello:hello,
	greet:greet
}
```

如果你对上面的写法十分困惑,那我们来分析下node的加载机制:

首先,node会把整个待加载的`hello.js`文件放入一个包装函数`load`中执行,在执行这个`load`函数前,node准备好了module变量:

```
var module={
	id:'hello',
	exports:{}
}
```

`load()`函数最终返回`module.exports`:

```
var load = function (exports,module){
	//hello.js文件内容
	...
	//load函数返回:
	return module.exports;	
};
var exported = load(module.exports,moudle);
```

默认情况下,node准备的`exports`变量和`module.exports`变量其实是同一个变量,并且初始化为空对象`{}`,于是我们可以这样玩:

```
exports.foo=function(){return 'foo';};
exports.bar=function(){return 'bar';};
```

也可以这样玩:

```
module.exports.foo=function(){return 'foo';};
module.exports.bar=function(){return 'bar'};
```

node准备了一个空对象`{}`,可以直接往里面加东西.

但是,如果要输出的是一个函数或数组,那么只能给`module.exports`赋值:

`module.exports=function(){return 'foo'};`	

给`exports`赋值是无效的,因为赋值后,`module.exports`仍然是空对象`{}`.

###结论
如果要输出一个键值对象`{}`,可以利用`exports`这个已存在的空对象`{}`,并继续在上面添加新的键值.

如果要输出一个函数或组,必须直接对`module.exports`对象赋值.
所以可以得出结论:直接对`module.exports`赋值,可以应对任何情况

```
module.exports={
	foo:function(){retun 'foo';}
}
```

或者

`module.exports=function(){return 'foo';};`

墙裂建议使用`module.exports='xxx'`的方式来输出模块变量.





























