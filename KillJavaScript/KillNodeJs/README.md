##*NodeJS 学习*

**个人学习NodeJS时的经历和总结 .**

>
***学习一项知识最好的办法是去使用它 .***


######学习资料
>- ***[菜鸟教程](www.runoob.com)***
- ***[廖雪峰](www.liaoxuefeng.com)***
- ***[NodeJS官网](www.nodejs.org)***

***绝大部分和原始材料相同,原意仅作学习摘抄,部分有自己添加的内容,能力有限,不保证是正确的.***

**持续更新中...**

>
 ***[GitHub](www.github.com/nzsghdx)***
 ***[码云](git.oschina.net/nzsghdx)***
 ***[网站](www.nzsghdx.com)***

