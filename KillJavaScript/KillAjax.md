## Kill Ajax

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillGit.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillGit.md?dir=0&filepath=KillGit.md&oid=32d1eb60ecc5bb585698096431465d7cb00f162e&sha=3e7efb7dbedb79b75133b8f09367a57795a28dee) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillGit.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy/src/3e7efb7dbedb79b75133b8f09367a57795a28dee/KillGit.md?at=master&fileviewer=file-view-default)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

---

### Updating . . .

- ***本文目录***
	- ***[实例代码](#shili)***

- ***Ajax:异步Javascript和xml***

- ***ajax是一种无需重新加载整个网页的情况下,能够更新部分网页的技术***

- ***ajax是一种用于创建快速动态网页的技术***

- ***通过在后台与服务器进行少量的数据交换,ajax可以使网页实现异步更新,这意味着可以在不加载整个网页的情况下,对网页的某部分进行更新***

- ***传统的网页如果需要更新内容,必须重新加载整个网页***

- ***ajax是基于现有的internet标准,并且联合使用他们***

- ***xmlhttprequest对象:异步的与服务器交换数据***
	- ***javascript/dom 信息显示交互***
	- ***css给数据定义样式***
	- ***xml作为转换数据的格式***

- ***ajax应用程序和浏览器和平台无关***

- ***ajax基于以下web标准***
	- ***javascript、xml，html，css***

- ***ajax优势***
	- ***web应用程序相比桌面应用程序有诸多优势***
	- ***他们能够涉及广大的用户，他们更易安装和维护，也更容易开发***

- ***ajax使用http请求***
	- ***传统的javascript使用http请求通过html表单的get或者post获取来自服务器的数据,但是服务器响应之后会重新加载页面，用户体验不好***
	- ***通过利用ajax，javascript会通javascript的xmlhttprequest对象，直接与服务器通信,用户不会注意到脚本在后台的请求***

- ***`xmlhttprequest`对象***  
	- ***通过使用xmlhttprequst对象，web开发者可以做到在页面已经加载完毕之后从服务器更新页面***

- ***`xmlhttprequest`对象的三个属性***  
  
	- ***`onreadystatechange` 处理服务器响应的函数***

		```
xmlhttp.onreadystatechange=function(){
//代码
	if(xmlhttp.readystate==4){
		//从服务器获得数据
	}
}
		```

	- ***readystate存有服务器状态的信息，每当readystate改变时，onreadystatechange就会执***行

		- ***readystate属性值***  
			- ***0 未初始化 在调用open（）之前***
			- ***1 请求已提出 调用send（）之前***
			- ***2 请求已发送 可以从响应得到内容头部***
			- ***3 请求处理中 部分数据可用，但是服务器还没完成响应***
			- ***4 请求已完成 可以访问数据并使用它***  

	- ***responsetext 取回服务器返回的数据***

- ***ajax请求服务器***
	- ***open（）和send（）方法***
		- ***open（）需要三个参数 第一个参数发送请求所使用的方法/第二个参数规定服务器脚本的url/第三个参数规定应当对请求进行异步处理***
		- ***send（）可将请求送往服务器***


### <span id = "shili">实例代码</span>

```
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script>
        function loadXMLDoc()
        {
            var xmlhttp;
            if (window.XMLHttpRequest)
            {
                //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
                xmlhttp=new XMLHttpRequest();
            }
            else
            {
                // IE6, IE5 浏览器执行代码
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET","/try/ajax/ajax_info.txt",true);
            xmlhttp.send();
        }
    </script>
</head>
<body>

<div id="myDiv"><h2>使用 AJAX 修改该文本内容</h2></div>
<button type="button" onclick="loadXMLDoc()">修改内容</button>

</body>
</html>
```


