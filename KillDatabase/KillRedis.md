## Kill Redis

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillRedis.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillRedis.md?dir=0&filepath=KillRedis.md&oid=e9f81d7b9c9bdf4aa0ae9e0dd79e2c245990ed95&sha=d31c24cfde5b04edc62b60fa78489f2739358fd5) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillRedis.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy/src/d31c24cfde5b04edc62b60fa78489f2739358fd5/KillRedis.md?at=master&fileviewer=file-view-default)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

---

>***代码托管库MarkDown语法标准不一样修改兼容起来还挺费劲的***

***学习笔记***

>参考资料 ***[RUNOOB](http://www.runoob.com/redis/redis-tutorial.html)***

***一些认为初学的时候容易迷糊的内容在本文最后***

本文结构:

- ***[特点及优势](#tdjys)***
- ***[Redis与其他key-value存储有什么不同](#RKbutong)***
- ***[Window 下安装](#RInstall)***
- ***[Redis 配置](#RConfig)***
- ***[Redis 数据类型](#RType)***
- ***[Redis 命令](#RCommand)***
- ***[其他](#qt)***
- ***[Redis与其他key-value存储有什么不同](#RKbutong)***
- ***[Redis与其他key-value存储有什么不同](#RKbutong)***
- ***[Redis与其他key-value存储有什么不同](#RKbutong)***
- ***[Redis与其他key-value存储有什么不同](#RKbutong)***


***Redis通常被称为数据结构服务器，因为值（value）可以是 字符串(String), 哈希(Map), 列表(list), 集合(sets) 和 有序集合(sorted sets)等类型.***

### <span id = 'tdjys'>特点及优势</span>
>***为什么要用它***

- ***免费开源.***
- ***支持数据的持久化，即可以将内存中的数据保存在磁盘中，重启的时候可以再次加载进行使用。***
- ***不仅支持简单的key-value类型的数据，同时还提供list，set，zset，hash等数据结构的存储。***
- ***支持数据的备份，即master-slave模式的数据备份。***
- ***性能极高 – 能读的速度是110000次/s,写的速度是81000次/s 。***
- ***丰富的数据类型 – 支持二进制案例的 Strings, Lists, Hashes, Sets 及 Ordered Sets 数据类型操作。***
- ***原子 – Redis的所有操作都是原子性的，同时还支持对几个操作全并后的原子性执行。***
- ***丰富的特性 – 支持 publish/subscribe, 通知, key 过期等等特性。***

### <span id = 'RKbutong'>Redis与其他key-value存储有什么不同</span>

- ***Redis有着更为复杂的数据结构并且提供对他们的原子性操作，这是一个不同于其他数据库的进化路径。Redis的数据类型都是基于基本数据结构的同时对程序员透明，无需进行额外的抽象。***
- ***Redis运行在内存中但是可以持久化到磁盘，所以在对不同数据集进行高速读写时需要权衡内存，因为数据量不能大于硬件内存。在内存数据库方面的另一个优点是，相比在磁盘上相同的复杂的数据结构，在内存中操作起来非常简单，这样Redis可以做很多内部复杂性很强的事情。同时，在磁盘格式方面他们是紧凑的以追加的方式产生的，因为他们并不需要进行随机访问。***

### <span id = 'RInstall'>Windows 下下载安装</span>

***[官方下载地址](https://github.com/MSOpenTech/redis/releases)***

***配置安装目录`pathhome\Redis`环境变量到`Path`***

***没配置环境变量的时可在一个命令行窗口执行`redis-server.exe redis.windows.conf`命令,然后在不关闭上一个命令窗口的前提下打开另一个命令行窗口测试能否设置并获取键值如下...:***

```
D:\tools\Redis>redis-cli.exe -h 127.0.0.1 -p 6379
127.0.0.1:6379> set myKey abc
OK
127.0.0.1:6379> get myKey
"abc"
```

***就当是测试是否安装成功了,如果配置环境变量后每次启动只需输入`redis-server.exe`,这部分`redis.windows.conf`使用默认的了.***


### <span id = 'RConfig'>Redis 配置</span>

***配置文件是在解压的根目录`redis.con`***

***根据名字可获取配置信息,输入*号可获取所有配置信息 命令如下:***

`redis 127.0.0.1:6379> CONFIG GET CONFIG_SETTING_NAME`

***例如***
>***输出信息第一行为参数名,第二行为值***

```
redis 127.0.0.1:6379> CONFIG GET loglevel

1) "loglevel"
2) "notice"

redis 127.0.0.1:6379> CONFIG GET *

  1) "dbfilename"
  2) "dump.rdb"
  3) "requirepass"
  4) ""
  5) "masterauth"
  6) ""
  7) "unixsocket"
  8) ""
  9) "logfile"
 10) ""
 11) "pidfile"
 12) "/var/run/redis.pid"
 13) "maxmemory"
 14) "0"
 15) "maxmemory-samples"
 16) "3"
 17) "timeout"
 18) "0"
 19) "tcp-keepalive"
 20) "0"
 21) "auto-aof-rewrite-percentage"
 22) "100"
 23) "auto-aof-rewrite-min-size"
 24) "67108864"
 25) "hash-max-ziplist-entries"
 26) "512"
 27) "hash-max-ziplist-value"
 28) "64"
 29) "list-max-ziplist-entries"
 30) "512"
 31) "list-max-ziplist-value"
 32) "64"
 33) "set-max-intset-entries"
 34) "512"
 35) "zset-max-ziplist-entries"
 36) "128"
 37) "zset-max-ziplist-value"
 38) "64"
 39) "hll-sparse-max-bytes"
 40) "3000"
 41) "lua-time-limit"
 42) "5000"
 43) "slowlog-log-slower-than"
 44) "10000"
 45) "latency-monitor-threshold"
 46) "0"
 47) "slowlog-max-len"
 48) "128"
 49) "port"
 50) "6379"
 51) "tcp-backlog"
 52) "511"
 53) "databases"
 54) "16"
 55) "repl-ping-slave-period"
 56) "10"
 57) "repl-timeout"
 58) "60"
 59) "repl-backlog-size"
 60) "1048576"
 61) "repl-backlog-ttl"
 62) "3600"
 63) "maxclients"
 64) "4064"
 65) "watchdog-period"
 66) "0"
 67) "slave-priority"
 68) "100"
 69) "min-slaves-to-write"
 70) "0"
 71) "min-slaves-max-lag"
 72) "10"
 73) "hz"
 74) "10"
 75) "no-appendfsync-on-rewrite"
 76) "no"
 77) "slave-serve-stale-data"
 78) "yes"
 79) "slave-read-only"
 80) "yes"
 81) "stop-writes-on-bgsave-error"
 82) "yes"
 83) "daemonize"
 84) "no"
 85) "rdbcompression"
 86) "yes"
 87) "rdbchecksum"
 88) "yes"
 89) "activerehashing"
 90) "yes"
 91) "repl-disable-tcp-nodelay"
 92) "no"
 93) "aof-rewrite-incremental-fsync"
 94) "yes"
 95) "appendonly"
 96) "no"
 97) "dir"
 98) "/home/deepak/Downloads/redis-2.8.13/src"
 99) "maxmemory-policy"
100) "volatile-lru"
101) "appendfsync"
102) "everysec"
103) "save"
104) "3600 1 300 100 60 10000"
105) "loglevel"
106) "notice"
107) "client-output-buffer-limit"
108) "normal 0 0 0 slave 268435456 67108864 60 pubsub 33554432 8388608 60"
109) "unixsocketperm"
110) "0"
111) "slaveof"
112) ""
113) "notify-keyspace-events"
114) ""
115) "bind"
116) ""
```

***编辑配置信息的话使用`CONFIG SET CONFIG_SETTING_NAME NEW_CONFIG_VALUE`如下:***

```
redis 127.0.0.1:6379> CONFIG SET loglevel "notice"
OK
redis 127.0.0.1:6379> CONFIG GET loglevel

1) "loglevel"
2) "notice"
```

***首先要搞懂参数的意思,早期可以先大概了解一下,如下:***

***redis.conf 配置项说明如下：***

- ***Redis默认不是以守护进程的方式运行，可以通过该配置项修改，使用yes启用守护进程***
    `daemonize no`
- ***当Redis以守护进程方式运行时，Redis默认会把pid写入/var/run/redis.pid文件，可以通过pidfile指定***
    `pidfile /var/run/redis.pid`
- ***指定Redis监听端口，默认端口为6379，作者在自己的一篇博文中解释了为什么选用6379作为默认端口，因为6379在手机按键上MERZ对应的号码，而MERZ取自意大利歌女Alessia Merz的名字***
    `port 6379`
- ***绑定的主机地址***
    `bind 127.0.0.1`
- ***当 客户端闲置多长时间后关闭连接，如果指定为0，表示关闭该功能***
    `timeout 300`
- ***指定日志记录级别，Redis总共支持四个级别：debug、verbose、notice、warning，默认为verbose***
    `loglevel verbose`
- ***日志记录方式，默认为标准输出，如果配置Redis为守护进程方式运行，而这里又配置为日志记录方式为标准输出，则日志将会发送给/dev/null***
    `logfile stdout`
- ***设置数据库的数量，默认数据库为0，可以使用SELECT <dbid>命令在连接上指定数据库id***
    `databases 16`
- ***指定在多长时间内，有多少次更新操作，就将数据同步到数据文件，可以多个条件配合
	`save <seconds> <changes>`***  
    ***Redis默认配置文件中提供了三个条件：***
    - `save 900 1`
    - `save 300 10`
    - `save 60 10000`  
    ***分别表示900秒（15分钟）内有1个更改，300秒（5分钟）内有10个更改以及60秒内有10000个更改。***
- ***指定存储至本地数据库时是否压缩数据，默认为yes，Redis采用LZF压缩，如果为了节省CPU时间，可以关闭该选项，但会导致数据库文件变的巨大***
    `rdbcompression yes`
- ***指定本地数据库文件名，默认值为dump.rdb***
    `dbfilename dump.rdb`
- ***指定本地数据库存放目录***
    `dir ./`
- ***设置当本机为slav服务时，设置master服务的IP地址及端口，在Redis启动时，它会自动从master进行数据同步***
    `slaveof <masterip> <masterport>`
- ***当master服务设置了密码保护时，slav服务连接master的密码***
    `masterauth <master-password>`
- ***设置Redis连接密码，如果配置了连接密码，客户端在连接Redis时需要通过AUTH<password>命令提供密码，默认关闭***
    `requirepass foobared`
- ***设置同一时间最大客户端连接数，默认无限制，Redis可以同时打开的客户端连接数为Redis进程可以打开的最大文件描述符数，如果设置 maxclients 0，表示不作限制。当客户端连接数到达限制时，Redis会关闭新的连接并向客户端返回max number of clients reached错误信息***
    `maxclients 128`
- ***指定Redis最大内存限制，Redis在启动时会把数据加载到内存中，达到最大内存后，Redis会先尝试清除已到期或即将到期的Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。Redis新的vm机制，会把Key存放内存，Value会存放在swap区***
    `maxmemory <bytes>`
- ***指定是否在每次更新操作后进行日志记录，Redis在默认情况下是异步的把数据写入磁盘，如果不开启，可能会在断电时导致一段时间内的数据丢失。因为 redis本身同步数据文件是按上面save条件来同步的，所以有的数据会在一段时间内只存在于内存中.默认为no***
    `appendonly no`
- ***指定更新日志文件名，默认为appendonly.aof***
     `appendfilename appendonly.aof`
- ***指定更新日志条件，共有3个可选值：***
	- `no`：表示等操作系统进行数据缓存同步到磁盘（快）
    - `always`：表示每次更新操作后手动调用fsync()将数据写到磁盘（慢，安全） 
    - `everysec`：表示每秒同步一次（折衷，默认值）
    - `appendfsync everysec`
 
- ***指定是否启用虚拟内存机制，默认值为no，简单的介绍一下，VM机制将数据分页存放，由Redis将访问量较少的页即冷数据swap到磁盘上，访问多的页面由磁盘自动换出到内存中（在后面的文章我会仔细分析Redis的VM机制）***
     `vm-enabled no`
- ***虚拟内存文件路径，默认值为/tmp/redis.swap，不可多个Redis实例共享***
     `vm-swap-file /tmp/redis.swap`
- ***将所有大于vm-max-memory的数据存入虚拟内存,无论vm-max-memory设置多小,所有索引数据都是内存存储的(Redis的索引数据 就是keys),也就是说,当vm-max-memory设置为0的时候,其实是所有value都存在于磁盘。默认值为0***
     `vm-max-memory 0`
- ***Redis swap文件分成了很多的page，一个对象可以保存在多个page上面，但一个page上不能被多个对象共享，vm-page-size是要根据存储的 数据大小来设定的，作者建议如果存储很多小对象，page大小最好设置为32或者64bytes；如果存储很大大对象，则可以使用更大的page，如果不 确定，就使用默认值***
     `vm-page-size 32`
- ***设置swap文件中的page数量，由于页表（一种表示页面空闲或使用的bitmap）是在放在内存中的，，在磁盘上每8个pages将消耗1byte的内存。***
     `vm-pages 134217728`
- ***设置访问swap文件的线程数,最好不要超过机器的核数,如果设置为0,那么所有对swap文件的操作都是串行的，可能会造成比较长时间的延迟。默认值为4***
     `vm-max-threads 4`
- ***设置在向客户端应答时，是否把较小的包合并为一个包发送，默认为开启***
   ` glueoutputbuf yes`
- ***指定在超过一定的数量或者最大的元素超过某一临界值时，采用一种特殊的哈希算法***

	```
	hash-max-zipmap-entries 64
    hash-max-zipmap-value 512
	```

- ***指定是否激活重置哈希，默认为开启（后面在介绍Redis的哈希算法时具体介绍）***
   	` activerehashing yes`
- ***指定包含其它的配置文件，可以在同一主机上多个Redis实例之间使用同一份配置文件，而同时各个实例又拥有自己的特定配置文件***
    `include /path/to/local.conf`


### <span id = 'RType'>Redis 数据类型</span>

>***Redis支持五种数据类型：string（字符串），hash（哈希），list（列表），set（集合）及zset(sorted set：有序集合)。***

***详情介绍如下:***

- ***String***
>***一个键最大能存储512MB。***
	- ***string是redis最基本的类型，你可以理解成与Memcached一模一样的类型，一个key对应一个value。***
	- ***string类型是二进制安全的。意思是redis的string可以包含任何数据。比如jpg图片或者序列化的对象 。***
	- ***string类型是Redis最基本的数据类型，一个键最大能存储512MB。***
	- ***实例***

	```
	redis 127.0.0.1:6379> SET name "runoob"
	OK
	redis 127.0.0.1:6379> GET name
	"runoob"
	```

- ***Hash***
>***每个 hash 可以存储 232 -1 键值对（40多亿）。***
	- Redis hash 是一个键值对集合。
	- Redis hash是一个string类型的field和value的映射表，hash特别适合用于存储对象。
	- 实例

	```
	127.0.0.1:6379> HMSET user:1 username runoob password runoob points 200
	OK
	127.0.0.1:6379> HGETALL user:1
	1) "username"
	2) "runoob"
	3) "password"
	4) "runoob"
	5) "points"
	6) "200"
	```

- ***List***
>***列表最多可存储 232 - 1 元素 (4294967295, 每个列表可存储40多亿)。*** 
	- ***Redis 列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）。***
	- ***实例***

	```
	redis 127.0.0.1:6379> lpush runoob redis
	(integer) 1
	redis 127.0.0.1:6379> lpush runoob mongodb
	(integer) 2
	redis 127.0.0.1:6379> lpush runoob rabitmq
	(integer) 3
	redis 127.0.0.1:6379> lrange runoob 0 10
	1) "rabitmq"
	2) "mongodb"
	3) "redis"
	redis 127.0.0.1:6379>
	```

- ***Set***
>***集合中最大的成员数为 2(32)幂 - 1约(4294967295), 每个集合可存储40多亿个成员)。***
	- ***Redis的Set是string类型的无序集合。***
	- ***集合是通过哈希表实现的，所以添加，删除，查找的复杂度都是O(1)。***
	- ***实例,命令为`sadd key member`,添加一个string元素到,key对应的set集合中，成功返回1,如果元素已经在集合中返回0,key对应的set不存在返回错误***

	```
	redis 127.0.0.1:6379> sadd runoob redis
	(integer) 1
	redis 127.0.0.1:6379> sadd runoob mongodb
	(integer) 1
	redis 127.0.0.1:6379> sadd runoob rabitmq
	(integer) 1
	redis 127.0.0.1:6379> sadd runoob rabitmq
	(integer) 0
	redis 127.0.0.1:6379> smembers runoob

	1) "rabitmq"
	2) "mongodb"
	3) "redis"
	```

>***我添加的时候出现了这个错误`(error) WRONGTYPE Operation against a key holding the wrong kind of value`.***
>>- ***后来原因找着了,因为没有事先创建set集合,先执行`set mykey "runoob"`再次执行上面的命令就行了***
- ***或者是和zset类型的集合名称冲突了,换一个就好了.***

- ***zset(sorted set：有序集合)***
	- ***Redis zset 和 set 一样也是string类型元素的集合,且不允许重复的成员。***
	- ***不同的是每个元素都会关联一个double类型的分数。redis正是通过分数来为集合中的成员进行从小到大的排序。***
	- ***zset的成员是唯一的,但分数(score)却可以重复。***
	- ***实例:添加元素到集合，元素在集合中存在则更新对应score,命令:`zadd key score member `***

	```
	redis 127.0.0.1:6379> zadd runoob 0 redis
	(integer) 1
	redis 127.0.0.1:6379> zadd runoob 0 mongodb
	(integer) 1
	redis 127.0.0.1:6379> zadd runoob 0 rabitmq
	(integer) 1
	redis 127.0.0.1:6379> zadd runoob 0 rabitmq
	(integer) 0
	redis 127.0.0.1:6379> ZRANGEBYSCORE runoob 0 1000
	
	1) "redis"
	2) "mongodb"
	3) "rabitmq"
	```

>***我添加的时候出现了这个错误`(error) WRONGTYPE Operation against a key holding the wrong kind of value`.和上面`sadd`添加的时候同样错误输出.***
>>***解决办法是把runoob换成其他的集合,可能是因为zset和set的集合名称冲突了,我试了一下当对方存在一个集合时,另外一个集合不能望那个集合中添加数据.***

### <span id = 'RCommand'>Redis 命令</span>
>***Redis 命令用于在 redis 服务上执行操作。
要在 redis 服务上执行命令需要一个 redis 客户端。Redis 客户端在我们之前下载的的 redis 的安装包中。执行命令之前需要先打开服务,新开一个命令行窗口输入以下命令`redis-server.exe`,不然就扯蛋了.***

- ***语法:Redis 客户端的基本语法为：***
`$ redis-cli`
- ***实例***
>***在以上实例中我们连接到本地的 redis 服务并执行 PING 命令，该命令用于检测 redis 服务是否启动***

	```
	$redis-cli
	redis 127.0.0.1:6379>
	redis 127.0.0.1:6379> PING

	PONG
	```
- ***远程命令语法*** `$ redis-cli -h host -p port -a password`
- ***实例***
>***以下实例演示了如何连接到主机为 127.0.0.1，端口为 6379 ，密码为 mypass 的 redis 服务上。***

	````
$redis-cli -h 127.0.0.1 -p 6379 -a "mypass"
redis 127.0.0.1:6379>
redis 127.0.0.1:6379> PING
PONG
```






># *Updating...*






### <span id = 'qt'>其他</span>
- ***`持久化`:可以理解为保存到磁盘中.持久即可以再使用,怎么样才能再次使用呢,保存到磁盘中就行了,像你保存到电脑本地的文件,随时打开都能用.***
- ***`守护进程`:更新中 . . .***



## 问题

<span id="set">***一直搞不懂在redis中set是设置的意思还是set集合的意思,如下***</span>

***如果set是set集合的意思,那么下面这句就是创建集合的例子:***  
***`set mykey nzsghdx`***
>***输入命令(先输入set)的时候他是这样提示的:set key value***

***mykey是集合nzsghdx的key***  
***nzsghdx是个set类型的集合,这个集合的名字就叫nzsghdx***    
***有句话叫:获取key值使用`get key`***  
***那么例如此例获取key值就是这样:get mykey***  
***得到的结果是nzsghdx***  
***那么nzsghdx就是key值***  
***而key值nzsghdx就是set集合的名字***  
***可以向nzsghdx里面添加数据(sadd)而不能向mykey中添加数据***  
***因为nzsghdx是个集合,而mykey只是个key***  
>***就是因为可以向nzsghdx里面添加数据,所以我这里才把他当成集合.***  


>***现在这里就有点懵了吧,和自己印象中的set集合不是一个套路啊,怎么set集合既有key又有value的样子.例如在java中,map集合才是key-value类型的***

-

***要是感觉乱的话,下面这样就清楚了:***


***创建一个名为nzsghdx的set集合.***
***set mykey nzsghdx***

***nzsghdx就是自己创建的集合,而mykey是集合名字(nzsghdx)的key.***

***向set集合中添加数据***
***sadd nzsghdx test1***

***test1就是自己添加的数据,而test1就是集合(nzsghdx)的value,而不是key(mykey)的value,mykey的value是(nzsghdx).***

***而set集合并不是key-value类型的,可通过sadd添加多个无序的内容到set集合中***
***set还是那个set,同样set集合中的内容还是无序的.key和value只是set集合名字(如nzsghdx)的属性***
***集合nzsghdx的key(mykey)和value(如test1),并不影响set集合(nzsghdx)的内容.***

***然而,问题来了***

***刚开始执行过`set mykey nzsghdx`之后,使用命令分别查看key和value的类型,发现***

![](http://oq0n46yk8.bkt.clouddn.com/redis1.png)  

***既然上面的nzsghdx是个集合了,为什么type nzsghdx输出为none呢.***


---

***那么这么理解,把set理解为设置的意思***  
***`set mykey nzsghdx`***  
***意思就是设置一个key名为mykey,而nzsghdx是mykey的value***
***因为我看到示例是这样子的"(SET KEY_NAME VALUE)"***  
***key有名字(mykey)也有值(nzsghdx)***    
***这里key的名字就是mykey,而nzsghdx是key(mykey)的值***    
***如果仅仅把mykey和nzsghdx当成键值对的话***  
***那么为什么可以向nzsghdx里面添加数据呢.***   
***nzsghdx不就是个key的值吗??...***     

***而且`type nzsghdx`的时候说明他不是个集合.***   
***所以以这种方式貌似理解不通.***


--
***小小理一下,因为可以nzsghdx中添加数据所以我把set命令当成创建一个set集合***  
***又因为刚开始的时候type类型是none,所以我把set命令当成好单纯不做作仅仅是设置个键值对的意思.***  
***但是给nzsghdx添加了数据之后,nzsghdx的类型就是set了.***  
***那么现总结如下:***
---


***上面的总结下呢就是:***  
***set mykey nzsghdx***  
***创建一个key为mykey,value为nzsghdx的键值对***  
***因为mykey的类型是string,nzsghdx的类型是none(意思就是什么类型都不是)***  
***当使用sadd nzsghdx test1命令给nzsghdx添加数据的时候***  
***nzsghdx的none类型转变为了set类型***  
***这时***  
***nzsghdx即是mykey的值也是set集合的名字***  
***那么`set mykey nzsghdx`的真正含义就是创建一个键为mykey名值为nzsghdx的键值对,当向值中通过什么方式添加什么数据的时候值就会变成相应的类型.***  
***嗯,应该就是这样***  


***不对,我感觉事情并不是那么简单.***  

***难道是因为***      
***type命令其实是(`type key`),`type value`(type nzsghdx)当然输出是none了??***  
***但是为什么给nzsghdx中添加数据之后使用`exists nzsghdx`或返回1呢.exists命令提示是这样子的:`exists key`,可是nzsghdx现在都是个集合了还返回1.nzsghdx的key不是mykey吗,nzsghdx不就是个value值吗,nzsghdx不就是个集合的名字吗,什么时候变成key了???***  
***给nzsghdx添加了数据之后,就变成key了,因为添加的命令是`sadd key`????***  

![叹气](http://oq0n46yk8.bkt.clouddn.com/redis.png)

***真叫人头大.***

![](http://oq0n46yk8.bkt.clouddn.com/%E7%9C%9F%E5%8F%AB%E4%BA%BA%E5%A4%B4%E5%A4%A7.jpeg)

