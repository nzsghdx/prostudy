## Kill Django

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy) [GitOsc](https://git.oschina.net/nzsghdx/ProStudy) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

***唉,写那个地址真是写烦了,写了好多个了也不想一个一个改了,弄成整个项目的地址吧..***

***参考自 [runoob](http://www.runoob.com/django/django-tutorial.html)***

***本文目录***

- ***简单介绍***
- ***简单上手***
- ***进阶***


### 简单介绍

***Django是Python下的一个成功的开放源代码的Web应用框架，采用了MVC的软件设计模式，即模型M，视图V和控制器C.***

### 简单上手

***前提 :***

- ***你要有个电脑,我用的是windows10***
- ***电脑要首先装好python,我的版本是python 3.5.0***
- ***django下载地址看这里 [djangoproject](https://www.djangoproject.com/download/)***

***开始玩吧***

### 安装

- ***把下载好的 django安装包解压到python的安装的根目录如`D:\tools\python`***
- ***然后执行`python setup.py install`命令***

### 验证安装是否成功

***首先要配置环境变量,不然怎么能愉快的玩耍***

***人家说需要配置这两个path环境变量***
`Python\Lib\site-packages\django;`
`Python\Scripts`.

***在python环境下逐次输入以下两条命令检查是否安装成功:***
`import django`
`print django.get_version()`
***如果输出了版本号就是安装成功了***

### 创建小项目

***可以先创建个文件夹,然后切到那个目录中,因为生成的项目就在该目录下.***

示例如下

```
django-admin.py startproject HelloWorld
```

>***django-admin.py是个命令,startproject顾名思义,HelloWorld是项目名***

***然后我出现了这个问题`django-admin.py不是内部或外部命令，也不是可运行的程序或批处理文件`,于是把django-admin.py的路径配置到了path环境变量中.然后就好了.***

***在浏览器中访问创建好的项目,首先要切换到创建好的项目目录下,然后执行命令如下***

```
python manage.py runserver 0.0.0.0:8000
```

***浏览器地址栏输入:`127.0.0.1:8000`***

***提示信息你看得懂的.***

***到此为止就算完成了django的安装即项目创建,下面就是写代码搞网站了.***

### 简单实例
>***需要两步,一个是用来显示内容的文件如`view.py`,一个是搞定路径的文件如`urls.py`.***

- ***新建项目view.py并添加以下内容***

```
from django.http import HttpResponse
 
def hello(request):
    return HttpResponse("Hello world ! ")
```

>***设置返回内容,网站文件应该写在这里.***

- ***在已有文件中更改全部代码为***

```
from django.conf.urls import url
 
from . import view
 
urlpatterns = [
    url(r'^$', view.hello),
]
```

>***用来设置项目的访问路径的.***

***然后像刚才那样启动django服务:`python manage.py runserver 0.0.0.0:8000`
浏览器访问:`127.0.0.1:8000`***

>***设置访问路径可更改代码为`url(r'^hello$', view.hello),`,那么访问路径就是`http://127.0.0.1:8000/hello`.***

***其实挺简单.多动手.开始了你就成功了一半.***


---

### Django 模型


***安装python-mysql驱动的时候`python setup.py install`出现错误***
`error: Microsoft Visual C++ 9.0 is required. Get it from http://aka.ms/vcpython27`

***然后根据相应提示信息进行解决,下载安装了VCForPython2.7.msi之后***

***继续执行`python setup.py install`然后出现了第二个问题***

```
error: command 'C:\\Users\\nzsghdx\\AppData\\Local\\Programs\\Common\\Microsoft\\Visual C++ for Python\\9.0\\VC\\Bin\\amd64\\cl.exe' failed with exit status 2
```

***找到这个解决办法***

```
I've seen this issue before, let me copy the relevant part from an isso issue:

The compiler cannot find the so-called "C (C99) headers", which don't appear to be bundled with your version of VC++. This affects the compilation of any software which needs the C99 headers, including Python libraries which have C99 extensions such as Misaka.

You can still download free, open-source versions of the headers. Here are some relevant links:
https://stackoverflow.com/questions/126279/c99-stdint-h-header-and-ms-visual-studio
http://deeplearning.net/software/theano/install_windows.html#visual-studio-and-cuda

The last link says:

Finally download the stdint.h header from here and save it as C:\Program Files (x86)\Common Files\Microsoft\Visual C++ for Python\9.0\VC\include\stdint.h.
```

***然而还是不行.还是有这个错误.***

```
error: command 'C:\\Users\\nzsghdx\\AppData\\Local\\Programs\\Common\\Microsoft\\Visual C++ for Python\\9.0\\VC\\Bin\\amd64\\cl.exe' failed with exit status 2
```

***于是我就在虚拟机里玩了:centos5.7***

***接下来..那就是我晚上的事情了.***

***Updating...***

