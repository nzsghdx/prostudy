## Kill Kaptcha

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillKaptcha.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillKaptcha.md) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillKaptcha.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

---

***Kaptcha听说是一个谷歌基于SimpleCaptcha搞出来的生成验证码的jar包.***

***怎么使用呢,两种方式如下***

- ***[普通jsp/servlet](#diyi)***
	- ***[进阶](#1jinjie)***
- ***[springmvc](#dier)***
	- ***[进阶](#2jinjie)***

### <span id="diyi">普通jsp/servlet</span>

>***需要编写两个文件,一个是web.xml一个是jsp页面,具体如下:***

- ***首先呢第一步要添加jar包,下载地址如下:[Maven库](http://mvnrepository.com/artifact/com.github.penggle/kaptcha)***

- ***在web.xml中添加如下代码:***

```
<!--Kaptcha 验证码  -->
    <servlet>  
        <servlet-name>kaptcha</servlet-name>  
        <servlet-class>com.google.code.kaptcha.servlet.KaptchaServlet</servlet-class>  
        <init-param>  
            <param-name>kaptcha.border</param-name>  
            <param-value>no</param-value>  
        </init-param>  
        <init-param>  
            <param-name>kaptcha.border.color</param-name>  
            <param-value>105,179,90</param-value>  
        </init-param>       
        <init-param>  
            <param-name>kaptcha.textproducer.font.color</param-name>  
            <param-value>red</param-value>  
        </init-param>  
        <init-param>  
            <param-name>kaptcha.image.width</param-name>  
            <param-value>250</param-value>  
        </init-param>  
        <init-param>  
            <param-name>kaptcha.image.height</param-name>  
            <param-value>90</param-value>  
        </init-param>  
        <init-param>  
            <param-name>kaptcha.textproducer.font.size</param-name>  
            <param-value>70</param-value>  
        </init-param>  
        <init-param>  
            <param-name>kaptcha.session.key</param-name>  
            <param-value>code</param-value>  
        </init-param>  
        <init-param>  
            <param-name>kaptcha.textproducer.char.length</param-name>  
            <param-value>4</param-value>  
        </init-param>  
        <init-param>  
            <param-name>kaptcha.textproducer.font.names</param-name>  
            <param-value>宋体,楷体,微软雅黑</param-value>  
        </init-param>       
    </servlet>  
    
<servlet-mapping>  
<servlet-name>kaptcha</servlet-name>
<!--访问地址(localhost:8080+项目名+设置的路径),访问路径如下:http://localhost:8080/Kaptcha_noSpringmvc/ClinicCountManager/kaptcha.jpg-->
<url-pattern>/ClinicCountManager/kaptcha.jpg</url-pattern>  
</servlet-mapping> 
```

- ***如果使用的是eclipse的话,为了让在web.xml中配置的路径有用,那么实际中要有这个路径才行,所以`WebContent/ClinicCountManager/kaptcha.jpg`即在`WebContent`下新建一个文件夹(floder)名为`ClinicCountManager`,然后再在`ClinicCountManager`中新建一个文件(file)名为`kaptcha.jpg`.如下***

	![kaptcha](image/kaptcha_floder.png)

- ***然后在浏览器中访问图片地址如:`http://localhost:8080/Kaptcha_noSpringmvc/ClinicCountManager/kaptcha.jpg`就好了,每次刷新都是个新图片.![ouyou](image/interesting/ouyou.png)***
>***想改什么设置可以直接在web.xml中尝试更改.***


---

### <span id="1jinjie">进阶</span>
>***首先进阶第一步,验证验证码.可以分为三步,第一步按照上述编写好上述代码,第二步增加验证代码,写个表格什么的,第三部也就是验证了,其实也就两步.***

- 写个jsp页面如下:

	```
	
	```



---

### <span id="dier">springmvc</span>
>***如果使用了spring框架的话就可以使用springmvc实现验证码更新了,而且上面那种方式也失效了,因为什么我给忘了.不过重点不是那个.***


---

### <span id="2jinjie">进阶</span>



