## *KillSsm*

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillSsm.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillSsm.md?dir=0&filepath=KillSsm.md&oid=43f927f9f69c89d6b281a2870db5178d23285d7c&sha=d31c24cfde5b04edc62b60fa78489f2739358fd5) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillSsm.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy/src/d31c24cfde5b04edc62b60fa78489f2739358fd5/KillSsm.md?at=master&fileviewer=file-view-default)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

---

>***以前也写过类似的,不过那时候懵懵懂懂,过段时间再看就感觉太菜了,用的多了感触就多了,为了更好的学习和复习,现记录如下.配置解析在最底部.***

>***后来遇到个好东西叫Maven.那就方便多了.***


- ***本文目录:***
	- ***[SSM各框架简介及作用](#introducton)***
	- ***[Spring使用范例](#spring)***
	- ***[SpringMVC使用范例](#springmvc)***
	- ***[Mybatis使用范例](#mybatis)***
	- ***[SSM整合使用范例](#zhenghe)***
	- ***[Maven整合SSM使用范例](#mzhenghe)***
	- ***[配置文件解析](#configuration)***
	- ***[常用注解](#zj)***
	- ***[其他](#qt)***


***ssm即spring-springmvc-mybatis***

- ***SSM各框架简介及作用***
	- ***Spring:***
		- ***Spring是一个轻量级的Java开源开发框架***
		- ***Spring是一个轻量级的控制反转（IoC）和面向切面（AOP）的容器框架.***
	- ***SpringMvc:***
		- ***Spring MVC属于SpringFrameWork的后续产品，已经融合在Spring Web Flow里面.***
		- ***Spring MVC 分离了控制器、模型对象、分派器以及处理程序对象的角色，这种分离让它们更容易进行定制.***
	- ***Mybatis:***
		- ***MyBatis 本是apache的一个开源项目iBatis.***
		- ***MyBatis是一个基于Java的持久层框架.***
		- ***MyBatis 消除了几乎所有的JDBC代码和参数的手工设置以及结果集的检索.***
		- ***MyBatis 使用简单的 XML或注解用于配置和原始映射，将接口和 Java 的POJOs（普通的 Java对象）映射成数据库中的记录.***


### *spring*
>***功能:依赖注入等***
>>***[jar包下载地址]()***
>>>***spring-framework貌似是向下兼容,有问题的话可以先看看配置文件和spring-farmework的版本是否一致或向下兼容.***

### *springmvc*
>***功能:页面跳转/数据传递等***
>>***[jar包下载地址]()***
>>>***spring-framework就包括spring和springmvc了***

### *mybatis*
>***功能:数据库操作等***
>>***[jar包下载地址]()***

----------

>- ***ssm是个成熟的框架,可以应付一般的web项目,下面是ssm每个部分的使用范例以及整合范例.***
- ***应该还需要个mybatis-spring的jar包.***

### *spring使用范例*

>***更多来自 [ImportNew](http://www.importnew.com/)***

- ***导入需要的jar包***

>***可以导入下载的全部jar包,也可以导入需要的最少jar包,以myeclipse为例,把jar文件复制到`src/WEB-INF/lib`目录下,然后`build path`→`Add to build path`.***

- ***编写配置文件***

>***配置文件为自己编写的xml文件,例如`Beans.xml`,用来配置信息,更改输出的时候可以直接在配置文件里面改动而不需要改写代码.***

```Beans.xml
<?xml version="1.0" encoding="UTF-8"?>
 
    <beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
 		http://www.springframework.org/schema/beans/spring-beans-3.0.xsd">
 		<!--class属性具体到类，‘com.inportnew’是包名，‘com.inportnew.HelloWorld’中的‘HelloWorld’是类名-->
       <bean id="helloWorld" class="com.importnew.HelloWorld">
           <property name="message" value="Hello World!"/>
       </bean>

    </beans>
```

- ***测试***

>***分别新建HelloWorld.java和MainApp.java类,然后运行,输出如下信息`Your Message : Hello World!`***

```HelloWorld.java
package com.importnew;
 
public class HelloWorld {
 
    private String message;
 
    public void setMessage(String message){
        this.message  = message;
    }
 
    public String getMessage(){
        return this.message;
    }
 
    public void printMessage(){
        System.out.println("Your Message : " + message);
    }
}
```

```MainApp.java
package com.importnew;
 
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
 
public class MainApp {
    public static void main(String[] args) {
 
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
 
        HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
 
        obj.printMessage();
    }
}
```

### *springmvc使用范例*
- ***导入需要的jar包***

>***可以导入下载的全部jar包,也可以导入需要的最少jar包,以myeclipse为例,把jar文件复制到`src/WEB-INF/lib`目录下,然后`build path`→`Add to build path`.***

- ***编写配置文件***

>***需要编写的配置文件包括`web.xml`和核心配置文件`springmvc-servlet.xml`,当然了,后期可以改进下配置文件.***

>***特别需要注意的是***

>- ***web.xml里面的<param-value>classpath:springmvc-servlet.xml</param-value>配置要检查一下和自己的配置文件名是否一致.***
- ***springmvc.xml里面的`<!-- scan the package and the sub package -->
<context:component-scan base-package="test.SpringMVC"/>`要配置成自己的,确保能够正确扫描到,不然就404了,这个错误我还是很难找的.***

```web.xml
<!--configure the setting of springmvcDispatcherServlet and configure the mapping-->
<servlet> 
	<servlet-name>springmvc</servlet-name> 
	<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class> 
	<init-param> <param-name>contextConfigLocation</param-name> 
		<param-value>classpath:springmvc-servlet.xml</param-value> 
	</init-param> 
	<!-- <load-on-startup>1</load-on-startup> --> 
</servlet> 
<servlet-mapping> 
	<servlet-name>springmvc</servlet-name> 
	<url-pattern>/</url-pattern> 
</servlet-mapping>
```

```springmvc-servlet.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" 
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
		xmlns:context="http://www.springframework.org/schema/context" 
		xmlns:mvc="http://www.springframework.org/schema/mvc" 
		xsi:schemaLocation="http://www.springframework.org/schema/beans 
		http://www.springframework.org/schema/beans/spring-beans.xsd 
		http://www.springframework.org/schema/context 
		http://www.springframework.org/schema/context/spring-context-4.1.xsd 
		http://www.springframework.org/schema/mvc 
		http://www.springframework.org/schema/mvc/spring-mvc-4.1.xsd"> 
<!-- scan the package and the sub package --> 
<context:component-scan base-package="test.SpringMVC"/> 
<!-- don't handle the static resource --> 
<mvc:default-servlet-handler /> 
<!-- if you use annotation you must configure following setting --> 
<mvc:annotation-driven /> 
<!-- configure the InternalResourceViewResolver --> 
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="internalResourceViewResolver"> 
	<!-- 前缀 --> 
	<property name="prefix" value="/WEB-INF/jsp/" /> 
	<!-- 后缀 --> 
	<property name="suffix" value=".jsp" /> 
</bean>
</beans>
```

- ***测试***

>***测试需要编写`controller.java`和`hello.jsp`,分别在`src`下新建`controller.java`在`src/WEB-INF/jsp`下新建`hello.jsp`,然后浏览器输入以下地址`http://localhost:8080/项目名/mvc/hello`查看.啥?不行,在`src/WEB-INF/jsp`下新建`index.jsp`了吗***

```controller.java
package test.SpringMVC;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mvc")
public class mvcController {
	@RequestMapping("/hello")//@RequestMapping("/hello")的hello是访问的路径
	public String hello(){
		//return "hello";中的hello是要跳转的页面名称,前后缀在配置文件中配置过了
		return "hello";
	}
}
```


### ***mybatis使用范例***

>***更多来自 [coolnameismy](http://liuyanwei.jumppo.com/)***

- ***导入需要的jar包***

>***可以导入下载的全部jar包,也可以导入需要的最少jar包,以myeclipse为例,把jar文件复制到`src/WEB-INF/lib`目录下,然后`build path`→`Add to build path`.***

- ***编写配置文件***

- ***一共包括两个配置文件,一个是`Mybatis.xml`,主要配置映射文件路径,数据库连接信息啥的***

>>***也可以使用`.properties`格式文件代替mybatis.xml中连接数据库的部分***  
>>***SSM整合了之后连mybatis.xml都不需要了***

```Mybatis.xml
<?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN" 
						"http://mybatis.org/dtd/mybatis-3-config.dtd">
    <configuration>
        <environments default="development">
            <environment id="development">
                <transactionManager type="JDBC" />
                <!-- 配置数据库连接信息 -->
                <dataSource type="POOLED">
                    <property name="driver" value="com.mysql.jdbc.Driver" />
                    <property name="url" value="jdbc:mysql://localhost:3306/test" />
                    <property name="username" value="root" />
                    <property name="password" value="xxx" />
                </dataSource>
            </environment>
        </environments>

        <mappers>
            <mapper resource="com/company/userMapper.xml"/>
        </mappers>

    </configuration>
```


- ***一个是编写动态sql语句的配置文件XXXMapper.xml***


>***需要特别注意的是:***
***XXXMapper.xml配置文件中的命名空间需要更改`<mapper namespace="com.nzsghdx.EmployeeMapper">`.***


```UserMapper.xml
<?xml version="1.0" encoding="UTF-8" ?>
    <!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
    <mapper namespace="com.nzsghdx.EmployeeMapper">

        <!--
            根据id查询得到一个user对象
         -->
        <select id="getEmployee" parameterType="int"
                resultType="com.nzsghdx.Employee">
            select * from employee where id=#{id}
        </select>
        
        <insert id="addEmployee" parameterType="com.nzsghdx.Employee">
            <!-- select * from employee where id=#{id} -->
            insert into employee(id,name,age) value (#{id},#{name},#{age})
        </insert>
        
        <update id="updateEmployee" parameterType="com.nzsghdx.Employee">
            <!-- select * from employee where id=#{id} -->
            update Employee set name=#{name},age=#{age} where id=#{id}
        </update>
        
        <delete id="deleteEmployee" parameterType="int">
            <!-- select * from employee where id=#{id} -->
            delete from employee where id=#{id}
        </delete>
    </mapper>
```



- ***测试***

>***测试文件包括三部分,需要一个普通java类如`User.java`,一个写有方法的接口文件如`UserMapper.java`,一个测试类如`Main.java`,运行`Main.java`输出如下信息`{“id”:1,name:”liuyanwei”}`***
>>- ***没有输出信息的话,你有数据库吗***
- ***还有上面那个映射文件XXXMapper.xml也要写好.***

```User.java
package com.nzsghdx;

public class Employee {
	private int id;
	private String name;
	private String age;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
	
}

```

```UserMapper.java
package com.nzsghdx;

public interface EmployeeMapper {
	public Employee getEmployee(int id) ;
	public void addEmployee(Employee employee) ;
	public void updateEmployee(Employee employee) ;
	public void deleteEmployee(int id) ;
}
```



```Main.java
public class Main {

        public static void main(String[] args) throws IOException {
          //mybatis的配置文件
            String resource = "conf.xml";
          //使用类加载器加载mybatis的配置文件（同时加载关联的映射文件）
            InputStream is = Main.class.getClassLoader().getResourceAsStream(resource);
		  //上句Main是当前类的类名.

		  //前两步亦可写成一句
			//InputStream is = Main.class.getClassLoader().getResourceAsStream("conf.xml");
		  //亦可使用MyBatis提供的Resources类加载mybatis的配置文件:别导错包了（同时加载关联的映射文件）
		    //Reader reader = Resources.getResourceAsReader("conf.xml");

          //构建sqlSession的工厂
            SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(is);
          //根据创建的sqlsessionFactory创建能执行映射文件中sql的sqlSession
          SqlSession session = sessionFactory.openSession();

			//下面注释的这几行我暂时不知道是干嘛的,先不用管他,可能不知道什么时候就理解了.
            /**
           * 映射sql的标识字符串，
             * me.gacl.mapping.userMapper是userMapper.xml文件中mapper标签的namespace属性的值，
             * getUser是select标签的id属性值，通过select标签的id属性值就可以找到要执行的SQL
		   * String statement = "com.company.User.getUser";//映射sql的标识字符串
             */
            
			
            UserMapper userMapper = session.getMapper(UserMapper.class);

			//CRUD
		
		//查询 retrieve
		/*try {
			Employee employee = mapper.getEmployee(1);
			System.out.println(employee);
		} catch (Exception e) {
			System.out.println("查询失败!");
			e.printStackTrace();
		}finally {
			ss.close();
		}*/
		
		//插入 creat
		/*try {
			Employee aEmployee = new Employee();
			aEmployee.setId(3);
			aEmployee.setName("test");
			aEmployee.setAge("3");
			mapper.addEmployee(aEmployee);
			ss.commit();
			System.out.println("插入的当前对象id是:"+aEmployee.getId());
		} catch (Exception e) {
			System.out.println("插入失败!");
			e.printStackTrace();
		}finally {
			ss.close();
		}*/
		
		
		//更新 update
		/*try {
			Employee uEmployee = mapper.getEmployee(3);
			uEmployee.setAge("update");
			uEmployee.setName("update");
			mapper.updateEmployee(uEmployee);
			ss.commit();
		} catch (Exception e) {
			System.out.println("更新失败!");
			e.printStackTrace();
		}finally {
			ss.close();
		}*/
		
		
		
		//删除 delete
		/*try {
			mapper.deleteEmployee(3);
			ss.commit();
		} catch (Exception e) {
			System.out.println("删除失败!");
			e.printStackTrace();
		}finally {
			ss.close();
		}*/

        }
    }
```   

>***如果使用不同的包名和类名的话,需要改的地方有***

>- ***操作sql的映射.xml文件如UserMapper.xml的namespace指向如:`<mapper namespace="com.nzsghdx.EmployeeMapper">
`***
- ***核心配置文件如Mybatis.xml离的的mapper属性的resource指向如:`com/nzsghdx/EmployeeMapper.xml`***


### *整合使用范例*

>***整合分为spring和mybatis的整合,spring和springmvc的整合,springmvc和mybatis的整合以及spring-springmvc-mybatis的整合.整合都是类似的,整合的是配置文件是配置,整合不是必要的,ssm整合如下.***
>>***更多来自 [CSDN](http://my.csdn.net/u012909091)***

- ***导入需要的jar包***

>***可以导入下载的全部jar包,也可以导入需要的最少jar包,以myeclipse为例,把jar文件复制到`src/WEB-INF/lib`目录下,然后`build path`→`Add to build path`.***

- ***编写配置文件***

>***首先是spring和mybatis的整合配置文件`spring-mybatis.xml`.***

```
<?xml version="1.0" encoding="UTF-8"?>  
<beans xmlns="http://www.springframework.org/schema/beans"  
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:p="http://www.springframework.org/schema/p"  
    xmlns:context="http://www.springframework.org/schema/context"  
    xmlns:mvc="http://www.springframework.org/schema/mvc"  
    xsi:schemaLocation="http://www.springframework.org/schema/beans    
                        http://www.springframework.org/schema/beans/spring-beans-3.1.xsd    
                        http://www.springframework.org/schema/context    
                        http://www.springframework.org/schema/context/spring-context-3.1.xsd    
                        http://www.springframework.org/schema/mvc    
                        http://www.springframework.org/schema/mvc/spring-mvc-4.0.xsd">  
    <!-- 自动扫描 -->  
    <context:component-scan base-package="com.cn.hnust" />  
    <!-- 引入配置文件 -->  
    <bean id="propertyConfigurer"  
        class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">  
        <property name="location" value="classpath:jdbc.properties" />  
    </bean>  
  
    <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"  
        destroy-method="close">  
        <property name="driverClassName" value="${driver}" />  
        <property name="url" value="${url}" />  
        <property name="username" value="${username}" />  
        <property name="password" value="${password}" />  
        <!-- 初始化连接大小 -->  
        <property name="initialSize" value="${initialSize}"></property>  
        <!-- 连接池最大数量 -->  
        <property name="maxActive" value="${maxActive}"></property>  
        <!-- 连接池最大空闲 -->  
        <property name="maxIdle" value="${maxIdle}"></property>  
        <!-- 连接池最小空闲 -->  
        <property name="minIdle" value="${minIdle}"></property>  
        <!-- 获取连接最大等待时间 -->  
        <property name="maxWait" value="${maxWait}"></property>  
    </bean>  
  
    <!-- spring和MyBatis完美整合，不需要mybatis的配置映射文件 -->  
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">  
        <property name="dataSource" ref="dataSource" />  
        <!-- 自动扫描mapping.xml文件 -->  
        <property name="mapperLocations" value="classpath:com/cn/hnust/mapping/*.xml"></property>  
    </bean>  
  
    <!-- DAO接口所在包名，Spring会自动查找其下的类 -->  
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">  
        <property name="basePackage" value="com.cn.hnust.dao" />  
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"></property>  
    </bean>  
  
    <!-- (事务管理)transaction manager, use JtaTransactionManager for global tx -->  
    <bean id="transactionManager"  
        class="org.springframework.jdbc.datasource.DataSourceTransactionManager">  
        <property name="dataSource" ref="dataSource" />  
    </bean>  
  
</beans>  
```

- ***测试***


---

### <span id = 'qt'>其他</span>

- ***分包管理***
	- ***com.nzsghdx.bean/com.nzsghdx.entity***

		>***存储bean,映射数据库中的表***
	- ***com.nzsghdx.dao***
	
		>***具体逻辑操作***
	- ***com.nzsghdx.service***

		>***接口文件***
	- ***com.nzsghdx.imple***

		>***接口实现包***
	- ***com.nzsghdx.controller***

		>***Servlet文件,基本逻辑操作,具体操作在dao层***
- ***重要名词释义***
	- ***`bean`***
		
		>- ***[极客学院](http://wiki.jikexueyuan.com/project/spring/bean-definition.html)被称作 bean 的对象是构成应用程序的支柱也是由 Spring IoC 容器管理的。bean 是一个被实例化，组装，并通过 Spring IoC 容器所管理的对象.这些 bean 是由用容器提供的配置元数据创建的.***
		>- ***[知乎](https://www.zhihu.com/question/19773379)***
			- ***1、所有属性为private***
			- ***2、提供默认构造方法***
			- ***3、提供getter和setter***
			- ***4、实现serializable接口***  

		>- ***我是在用spring的时候迷糊的,现在理解的呢就是`一个具有上面四条性质的类被实例化后的那个对象`,用在spring注解中就是`一个具有上面四条性质的类被实例化后且被 Spring IoC 容器所管理的那个对象`***


### <span id = 'configuration'>配置文件解析</span>

***官方有句话是这么说的:  
`Authoring Spring configuration files using the older DTD style is still fully supported.`
意即老的配置文件也支持.下面是官方示例:***

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN 2.0//EN"
        "http://www.springframework.org/dtd/spring-beans-2.0.dtd">

<beans>

<!-- bean definitions here -->

</beans>
```

***`The equivalent file in the XML Schema-style would be…`
意即也可以如下这么写:***

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- bean definitions here -->

</beans>
```

***`The 'xsi:schemaLocation' fragment is not actually required, 
but can be included to reference a local copy of a schema (which can be useful during development).  `
意即`'xsi:schemaLocation'`不是必须的,但是用了后开发的时候有好处.***

---

***下面是一个使用util schema的小例子:***

***要使配置生效配置文件首先要这么写:***

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:util="http://www.springframework.org/schema/util" xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd"> <!-- bean definitions here -->

</beans>
```

***和原始配置文件相比,多了***

```
xmlns:util="http://www.springframework.org/schema/util" xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd"***
```
		
>***xmlns后面首先跟了util路径然后又跟了schemaLocaltion路径  
'uilt'是指定要用的注解名称,随后跟的是该注解的网络路径  
'xsi:schemaLocation'指定的是util注解启动及其依赖文件路径,然后util注解才能生效***

- ***使用util的constand(常量):\<util:constant/>例子如下***

	***使用\<util:constant/>之前的bean配置:***

	```
	<bean id="..." class="...">
    	<property name="isolation">
        	<bean id="java.sql.Connection.TRANSACTION_SERIALIZABLE"
                	class="org.springframework.beans.factory.config.FieldRetrievingFactoryBean" />
    	</property>
	</bean>
	```

	***使用之后(更干净简洁):***

	```
	<bean id="..." class="...">
    	<property name="isolation">
        	<util:constant static-	field="java.sql.Connection.TRANSACTION_SERIALIZABLE"/>
    	</property>
	</bean>
	```

	***想一下既然把"java.sql.Connection.TRANSACTION_SERIALIZABLE"放到静态域中了,使用bean时就不需要在指定一下了.***
***因为在加载类的时候就在指定域中生效了.***

>***解释如下:***

>- ***FieldRetrievingFactoryBean是FactoryBean用来检索静态或非静态field value的.*** 
>- ***用来检索public static final语句定义的常量, 或许可能会为另外一个bean设置一个变量或构造器标签.***
>- ***反正就是一个用来检索常量的类.这个不重要.***


- ***spring的方便之处不用多说:下面是个注入到spring的小例子:***
	- ***一个枚举类:***

	```
	package javax.persistence;

	public enum PersistenceContextType {

    	TRANSACTION,
    	EXTENDED

	}
	```

	- ***一个带有setter方法的类:***

	```
	package example;

	public class Client {

    	private PersistenceContextType persistenceContextType;

    	public void setPersistenceContextType(PersistenceContextType type) {
        	this.persistenceContextType = type;
    	}

	}
	```


	***那么spring配置文件中的注入信息如下:***

	```
	<bean class="example.Client">
    	<property name="persistenceContextType" value="TRANSACTION" />
	</bean>
	```

	>***persistenceContextType是example.Client类中的一个属性,  
value="TRANSACTION"中的TRANSACTION是persistenceContextType枚举类中的一个值.***


>***更多请查阅[官方文档]().  
需要用到什么的时候再去查有关资料工作会更有效率.涉猎太广影响研究深度.  
如果你认为你有能力跟那么多骚气的技术斗,那他们有一百种方法让你在计算机领域呆不下去.  
当然了要与时俱进,出来的新技术新玩意你不想动手玩一下吗.***


---


### <span id = 'zj'>常用注解</span>
	
>***注解是啥玩意有什么用呢 : 比没有强.***  
***看到一句话感觉说的很好:注解实现Bean配置主要用来进行如依赖注入、生命周期回调方法定义等，不能消除XML文件中的Bean元数据定义，且基于XML配置中的依赖注入的数据将覆盖基于注解配置中的依赖注入的数据.***  
***有篇文章感觉总结的很好 [看这里](http://blog.csdn.net/giserstone/article/details/17173929)***

- ***`Controller`:标注控制层组件,写到类上面,就归spring管了***
- ***`Component`/`Repository`/`Service`和`Controller`类似,人家说不推荐使用`Component`,貌似是因为其他三个比他更细化.***
	- ***`Component`:组件不好归类,可使用此注解***
	- ***`Service`:标注业务层组件***
	- ***`Repository`:标注数据访问组件,即dao组件***
- ***`RequestMapping`:访问此注解下面的内容加上注解内的路径***
- ***`Qualifier`:限定描述符***
- ***`Autowired`:属性加上此注解,就不需要getter()和setter()方法,spring自动注入.用于替代xml配置的自动装配,默认根据类型注入***
	- ***要使@Autowired能够工作，还需要在配置文件中加入以下代码.***
		- ***`<bean class="org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor" />`***
- ***`Resource`:自动装配,需引入`com.annotation.jar`***
	- ***xml配置文件如下:***
	
		```
		<beans xmlns="http://www.springframework.org/schema/beans"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns:context="http://www.springframework.org/schema/context"
			xsi:schemaLacation="http://www.springframework.org/schema/beans
				http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
				http://www.springframework.org/schema/context
				http://www.springframework.org/schema/context/spring-context-2.5.xsd">
				<context:annotation-config/>
		</beans>
		```

- ***`Required`:依赖检查***
- ***`Scope`:定义bean的作用范围***

---

>- ***开启注解驱动支持写入如下代码:使用\<context:annotation-config />简化配置***

```
<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"    
    xsi:schemaLocation="http://www.springframework.org/schema/beans    
    http://www.springframework.org/schema/beans/spring-beans-2.5.xsd    
    http://www.springframework.org/schema/context    
    http://www.springframework.org/schema/context/spring-context-2.5.xsd">    
    <context:annotation-config />    
</beans>  
```

>>- ***注解本身是不会做任何事情的，它仅提供元数据信息。要使元数据信息真正起作用，必须让负责处理这些元数据的处理器工作起来.要使元数据信息真正起作用，必须让负责处理这些元数据的处理器工作起来。 
AutowiredAnnotationBeanPostProcessor和CommonAnnotationBeanPostProcessor就是处理这些注释元数据的处理器。但是直接在Spring配置文件中定义这些Bean显得比较笨拙。Spring提供了一种方便的注册这些BeanPostProcessor的方式，就是\<context:annotation-config />***

>>> - ***\<context:annotationconfig />将隐式地向Spring容器注册AutowiredAnnotationBeanPostProcessor、CommonAnnotationBeanPostProcessor、 PersistenceAnnotationBeanPostProcessor以及RequiredAnnotationBeanPostProcessor这4个BeanPostProcessor.***

---

> - ***使用\<context:component-scan />让Bean定义注解工作起来,配置如下:***

```
<beans xmlns="http://www.springframework.org/schema/beans" 		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 		xmlns:context="http://www.springframework.org/schema/context"    
    	xsi:schemaLocation="http://www.springframework.org/schema/beans    
    	http://www.springframework.org/schema/beans/spring-beans-2.5.xsd    
    	http://www.springframework.org/schema/context    
    	http://www.springframework.org/schema/context/spring-context-2.5.xsd">    
    <context:component-scan base-package="com.kedacom.ksoa" />    
</beans>   
```


>- ***\<context:component-scan />的base-package属性指定了需要扫描的类包，类包及其递归子包中所有的类都会被处理*** 
- ***\<context:component-scan />还允许定义过滤器将基包下的某些类纳入或排除***

### <span id = "question">问题</span>

- ***Mybatis操作数据库的时候,例如查询调用的是接口中定义的方法,是怎么定位到mapper.xml文件中的sql语句的,是根据判断mapper.xml中的id值和接口中的方法名是否相同确定的吗?***

>***写代码动手试一下就知道了...***


spring引入配置文件之后
然后跟spring有关的配置文件下,都可以直接使用已经引入的配置文件
例如在spring.xml中引入
 <!-- 引入属性文件 -->  
    <context:property-placeholder location="classpath:db.properties" />  
然后在spring-mybatis.xml里面直接使用,不用再次引入
 <!-- 配置数据源 -->  
    <bean name="dataSource" class="com.alibaba.druid.pool.DruidDataSource" init-method="init" destroy-method="close">  
        <property name="url" value="${jdbc_url}" />  
        <property name="username" value="${jdbc_username}" />  
        <property name="password" value="${jdbc_password}" />  
    </bean>  
不知道是不是因为在web.xml里面配置了这个
<init-param>  
            <param-name>contextConfigLocation</param-name>  
            <param-value>classpath:spring*.xml</param-value>  
        </init-param>  
或者仅仅是那个druid的jar包比较特殊(出现了alibaba字样)

${pageContext.request.contextPath}
