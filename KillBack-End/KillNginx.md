## Kill Nginx

### Nginx

***Nginx是一款面向性能设计的HTTP服务器，相较于Apache、lighttpd具有占有内存少，稳定性高等优势。***


>***感谢那么多坑让我离大佬越来越近.***

- ***启动***
- ***停止***
- ***重启***

### 启动nginx
***使用配置nginx执行配置文件***  

***`nginx -c /usr/local/nginx/conf/nginx.conf`***  

***不行的话就切入到nginx的安装sbin目录执行,例如***

***`/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf`***

>***`-c`参数是用来启动的.***

>***测试nginx配置文件修改之后是否存在错误***  
***切入到nginx的安装目录,找到配置文件执行命令***  
***例如***  
***`nginx -t /usr/local/nginx/conf/nginx.conf`*** 
 
>***或者切入到nginx的sbin目录执行`./nginx -t`***  
***或者随便哪个目录直接执行`nginx -t`***  
***如果出现如下输出就证明没有错误***  

>```
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful
```

>***还有一个方法就是在启动命令的`-c`参数前加上测试参数`-t`,例如***
***`nginx -t -c /usr/local/nginx/conf/nginx.conf`***



### 停止nginx

***首先使用`ps -ef | grep nginx`命令查看nginx的进程号***

- ***从容停止***
	***`kill -QUIT 进程号`***
- ***快速停止***
	***'kill -TERM 进程号'***
	***'kill -INT 进程号'***
- ***强制停止***
	***`pkill -9 nginx`***


### 重启
>- ***也可以先关闭再启动.***
- ***在关闭状态下无法重启.***

***两种方式***  

- ***一种是执行`nginx -s reload`***  
***不行的话切入到nginx的sbin目录执行此命令`nginx -s reload`***  
***或者直接`/usr/local/nginx/sbin/nginx -s reload`***

- ***第二种是使用`kill -HUP 进程号`命令***


### nginx反向代理

***nginx的安装目录在这里`/usr/local/nginx/`***
***配置文件在这个路径`/usr/local/nginx/conf/`***
>***根据自己的安装位置寻找配置文件路径.***

***设置反向代理需要更改`nginx.conf`文件.打开***  
***大概结构是下面这个样子结构的.***

```
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    server {
        listen       80;
        server_name  127.0.0.1:8080;
        location / {
            proxy_pass   http://127.0.0.1:8080;
        }
    }

}
```

**例如现在有个需求:因为某个网站需要加上端口号9000才能访问网站如www.nzsghdx.com:9000,现需要不增加端口号访问网站.这里就需要用到nginx的反向代理了.***

>**先小小的分析一下,http默认端口是80,https默认端口是443,浏览器默认不显示默认端口号,例如需要使用http方式访问网站,需要做如下设置:**


```
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    server {
        listen       80;
        server_name  www.nzsghdx.com;
        location / {
            proxy_pass   http://www.nzsghdx.com:9000;
        }
    }

}
```

***名词释义***

- ***listen：监听某个端口,我暂时不知道是干嘛的***
- ***server_name：转发到的那个地址***
- ***proxy_pass：代理哪个地址***

>***因为nginx本身使用的就是127.0.0.1(本机地址),网站www.nzsghdx.com部署到了127.0.0.1,所以`server_name  www.nzsghdx.com`就表示访问www.nzsghdx.com就到了服务器的本机地址127.0.0.1.经过配置之后,之前需要加上端口号9000才能访问本机地址现在不需要了***
>>***也可把`server_name  www.nzsghdx.com`写成`server_name  www.nzsghdx.com:80`,因为本机地址默认使用80端口,所以可以不写,写了浏览器也不会显示的,因为是默认的.***

### 更多玩法正在探索中