## Kill Http

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillGit.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillGit.md?dir=0&filepath=KillGit.md&oid=32d1eb60ecc5bb585698096431465d7cb00f162e&sha=3e7efb7dbedb79b75133b8f09367a57795a28dee) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillGit.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy/src/3e7efb7dbedb79b75133b8f09367a57795a28dee/KillGit.md?at=master&fileviewer=file-view-default)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

---

>***当时摘抄的时候忘了是哪本书了,现记录如下,修改更新中 . . .***

- ***目录索引如下:***
	- ***[Http是什么](#what)***
	- ***[Http的简单应用](#simpleU)***
	- ***[Http的特点](#td)***
	- ***[Http简单解析](#jx)***
	- ***[Http中的术语](#sy)***
	- ***[Http规范](#gf)***
	- ***[Http面试题](#ms)***
	- ***[Updating . . .]()***

--


### <span id = 'td'>Http2特点</span>

***http1在指定的tcp连接上同一时刻只允许执行一个请求*** 
 
- ***虽然http1.1增加了管道,但是这只是解决了部分请求并发的问题,却仍然对报头阻塞所困***
	- ***因为报头阻塞,所以http1和http1.1只能依靠和服务器建立多个连接的策略来实现并发效果,降低时延***  
	- ***http头部字段经常重复和冗余,引起不必要的网络阻塞,并且快速的填满了初始的tcp阻塞窗口***  
	- ***http2通过优化http语义传输解决了tcp阻塞的问题***  
- ***http2允许请求和响应信息在同一个连接上交错传递,并且对http头部提供了一套高效的编码规则***  
- ***http2赋予请求优先级,让重复的请求优先完成,从而进一步提升性能***  
- ***http2语义对网络更友好,相对于http1他需要更少的tcp连接,减少连接资源竞争,正常的连接生命周期***  
- ***http2允许使用二进制消息帧以提高消息的处理效率***  
- ***http2支持所有http1的核心特征,并且在有些地方做得更高效***  
- ***http2的基本协议单元是帧,每个帧类型都有不同的目的和用途***  
- ***报头帧和数据帧组成了基本的http请求和响应***  
- ***请求多路复用是通过将http请求响应的交换关联到一个流上来实现的***  
- ***因为流之间是相互独立的,所以一个流上的阻塞或停止不会影响到其他流***  
- ***流量控制和优先级保证了可以高效的使用多路复用流***  
	- ***流量控制有助于保证只有可以被接受者使用的数据才会被传输***  
- ***优先级使得最重要的流可以优先使用有限的资源***  
- ***http2添加了一种新的交互模式,这种交互模式中服务器可以向客户端推送响应***  
	- ***服务器推送允许服务器根据预测客户端可能需要的数据,主送向客户端发送数据,用一点网络的使用来交换潜在的时延降低***  
	- ***服务器会通过合成一个通过push-promise帧发送的请求来实现推送,然后服务器就可以在一个单独的流中响应这个合成请求***  
- ***因为一个连接中的http报头字段可能包含大量的冗余数据,所以包含报头字段的帧是被压缩的***  
	- ***通常情况下压缩能够极大的减少请求大小,从而实现将多个请求放入一个包里***  

--

- ***<span id = 'gf'>http2规范分为四个部分</span>***

	- ***1.启动http2包含了一个http2连接是如何初始化的***
	- ***2.分帧和流层描述了http2的帧是如何构成复用流的***
	- ***3.帧和错误定义包括了http2中使用的帧和错误类型的详细内容***
	- ***4.http2寻址和附加需求描述了如何用帧和流表达http语义***

>- ***因为一些帧和流层的概念是和http分离的,所以这个规范没有定义一个完整通用的帧层***
- ***帧和流层是根据http协议和服务器推送的需要定制的***

- ***<span id = 'sy'>术语解释:</span>***

	- ***客户端:发起http2请求的端点,客户端发送请求接收响应***
	- ***连接:在两个端点之间的传输层级别的连接***
	- ***连接错误:影响整个http2连接的错误***
	- ***端点:连接的客户端或服务器***
	- ***帧:http2通信连接中的最小单元,包括根据帧类型结构的字节的报头和可变长度的序列***
	- ***对等段:一个端点,当讨论待定的端点时,对等段指的是讨论的主题的远程端点***
	- ***接收端:正在接收帧的端点***
	- ***发送端:正在传输帧的端点***
	- ***服务端:接受http连接的端点,服务器接受请求发送响应***
	- ***流:http2连接中的一个双向字节帧流***
	- ***流错误:一个http2流中的错误***

--

### <span id = 'jx'>Http简单解析</span>

- ***http2连接是运行在tcp连接上的应用层协议,客户端是tcp连接的发起者***
- ***http2使用和http1.1一样的http和https的URL***
- ***http也使用同样的默认端口,http使用的默认端口是80,https使用的默认端口是442***
- ***http类型的url和https类型的url在判断上游是否支持http2的方法是不一样的***

- ***本文定义的协议有两个标识符***
	- ***h2表示http2协议使用tls 这个标示在tls-alpn字段中和任何基于tls的http2协议中***
	- ***h2以练歌十六进制序列 0x86 0x32形式序列化到alpn协议标识符中***
	- ***h2c表示http2协议运行在明文tcp上 这个标识符用在http1.1升级报头字段中以及任何基于tcp的http2协议中***
	- ***h2c是从alpn标识符中保留下来的,但是它描述的是不适用tls的协议***

- ***在不知道下一跳是否支持http2的情况下,客户端使用http升级机制来向http的url发送请求***
	- ***客户端构造一个升级报头字段值为h2c的请求做到这一点,这样http1.1请求必须且只能包含一个http2-setting头部字段***
	- ***例如以下:***

		```
GET /HTTP/1.1
Host: server.example.com
Connection:Upgrade,HTTP2-Settings
Upgrade:h2c
HTTP2-Settings:<base64url encoding of HTTP/2 SETTINGS payload>
		```

- ***客户端必须在全部发送完包含主题内容请求之后才能发送http2帧***
	- ***这意味着一个大的请求在完全发送完之前可以阻塞连接***
	- ***如果一个初始请求后的后续请求并发性很重要,则可以用一个额外的请求将协议升级到http2,代价是多一个来回的时间***
	- ***不支持http2的服务器也可以回复升级请求,只不过响应不包括升级报头字段,例如以下:***

		```
HTTP/1.1 200 OK
Content-Length:243
Content-Type:text/html
		```

- ***服务器必须忽略升级报头中的h2,h2的存在意味着这是基于tls实现的http2***
	- ***一个支持http2的服务器接收升级请求并返回响应101(切换协议)***
	- ***在发送空行表示101响应结束后,服务器就可以开始发送http2帧了,这些帧必须包含对发起升级请求的响应***
	- ***例如以下:***

		```
HTTP/1.1 101 Switching protocols
Connection:Upgrade
Upgrade:h2c
[HTTP/2 connection...
		```

>- ***服务器发送的第一个http2帧必须是包含一个Settings帧的服务器连接前言***
- ***一旦接收101响应,客户端也必须发送一个包含settings帧的连接前言***
- ***在升级之前发送的http1.1的请求被分配一个标志为1的流并赋予默认优先级***
- ***流1是从客户端到服务器是隐式半封闭的,因为这个请求是按照http1.1协议请求完成的***
- ***在http2连接完成后,流1将被用来发送响应***
- ***升级http1.1到http2的升级请求必须又切只能有一个http2-settings报头字段***
- ***http2-settings报头字段是一个连接特定报头字段,它包含控制http2连接的参数,提供该参数期望服务器能够接受升级请求***
- ***如果http2-settings报头字段没有提供或者提供了不止一个,服务器不应该升级连接到http2 另外,服务器不能发送这个报头字段***

- ***http2-settings报头字段的内容是settings帧的载体,被编码成base64url格式的字符串***
	- ***因为升级是对当前连接而言的,所以发送http2-settings报头字段的客户端也必须在Connection报头字段中发送http2-settings来防止请求被转发***
	- ***例如以下:***

		```
GET / HTTP/1.1
Host:server.example.com
Connection:Upgrade,HTTP2-Settingspgrade:h2c
HTTP2-Settings:<base64url encoding of HTTP/2 SETTINGS payload>
		```

>- ***服务器会像对任何其他Settings帧一样解码和解释这些值,因为101响应是作为一个隐式确认***
- ***所以没有必要对这些设置进行显式确认,在升级请求中提供这些值赋予了客户端在收到任何来自服务器的帧之前提供这些参数的集机会.***


--

- ***<span id = 'ms'>面试之http***</span>

	- ***1.什么是http协议***  

		>***超文本传输协议，可以快速的从全球的web服务器将各种信息迅速边界可靠的搬移到浏览器中***  
	- ***2.web客户端是什么***  
		>***各大厂商的浏览器***  
	- ***3.web服务端是什么***    
		>***web服务器存储web类容。web服务器使用http协议，称为http服务器***  
	- ***4.什么是资源***  
		>***所有能够提供web内容的东西都是web资源***  
	- ***5.mime类型是什么***  
		>***多用途因特网邮件扩展，mime类型是一种文本标记，标志一种主要的对象类型和一个特定的子类型，中间由一条斜杠分割***  
	- ***6.什么是uri***  
		>***统一资源标识符，包括url和urn***  
	- ***7.什么是url***  
		>- ***统一资源占位符，三部分组成：协议/端口号/地址***  
		>- ***几乎所有的uri都是url***  
	- ***8.什么是urn***  
		>***统一资源名***  
	- ***9.常见的http方法有哪些***  
		>- ***get 从服务器向客户端发送命名资源***  
		>- ***put 将来自客户端的数据存储到一个命名的服务器中***  
		>- ***delete	从服务器中删除命名资源***  
		>- ***post 将客户端数据发送到要给服务器网关应用程序***  
		>- ***head 仅发送资源响应中的http头部***  
	- ***10.常见状态码的含义***  
		>- ***200 ok 成功，请求的所有数据都在响应主体中***
		>- ***206 partial content 成功的执行了一部分或者range/范围请求***
		>- ***302 found 重定向，到其他地方获取资源***
		>- ***304 not modified 如果客户端发送了一个get请求，二资源最近未被修改，则用304说明未被修改***
		>- ***305 use proxy 必须通过代理访问***
		>- ***403 forbidden 请求被服务器拒绝***
		>- ***404 not found 无法找到请求的url***
		>- ***500 internal server error 服务器遇到一个妨碍它为请求提供服务的错误***
		>- ***503 bad gateway 服务器现在无法为请求提供服务，但是将来可以***
	- ***11.什么是报文***  
		>- ***http报文是由一行一行的简单字符串组成的***  
		>- ***http报文是纯文本的，不是二进制代码***  
		>- ***请求报文：从web客户端发往web服务器的http报文称为请求报文***  
		>- ***响应报文：从web服务器发往客户端的报文称为响应报文***  
		>- ***http报文分为以下三个部分：***  
			- ***起始行：报文的第一行就是起始行，在请求报文中说明要做些什么，在响应报文中说明出现了什么问题***  
			- ***首部字段：起始行后面由零个或多个首部字段，以键值对的形式表示首部字段 首部以一个空行结束***  
			- ***主体：首部字段空行之后就是可选的报文主体，其中包含了所有类型的数据，请求主体中包含了要发往web服务器的数据，响应主体中则是要返回客户端的数据***  
	- ***12.http协议栈是怎么样的***  
		>- ***http是应用层协议，他把联网的细节都交给了通用的可靠的tcp/ip协议***  
		>- ***http网络协议栈***  
			- ***http 应用层***  
			- ***tcp 传输层***  
			- ***ip 网络层***  
			- ***网络特有的链路接口 数据链路层***  
			- ***物理网络硬件 物理层***  
		>- ***tcp协议：***  
			- ***传输控制协议***  
			- ***无差错的数据传递***  
			- ***按序传递（数据总会按照发送的顺序到达）***  
			- ***未分段的数据刘（可以在任意时刻任意尺寸将数据发送出去）***  
	- ***13.什么是dns***
		>***域名解析服务 将主机名解析为ip地址***
	- ***14.什么是端口号***
		>***答案说了像没说一样 “80就是端口号” MDZZ***
	- ***15.通过浏览器访问服务器资源的流程***
		>- ***1.浏览器从url中解析出服务器的主机名***
		>- ***2.将主机名解析为ip地址***
		>- ***3.浏览器将端口号从url中解析出来***
		>- ***4.浏览器建立一条与web服务器的tcp连接***
		>- ***5.浏览器向浏览器发送一条http请求报文***
		>- ***6.服务器向浏览器发送一条http响应报文***
		>- ***7.关闭连接，浏览器显示文档、***
	- ***16.什么是代理***
		>***代理就是处于客户端和服务器之间的http中间实体，接受所有的http请求并转发给服务器（可能会将请求修改之后转发）***
	- ***17.什么是缓存***
		>***缓存http的仓库，使常用的页面的副本存在离客户端更近的地方***
	- ***18.什么是网关***
		>***网关是一种特殊的服务器，作为其他服务器的中间实体使用，通常用于将http流量转换为其他协议***
	- ***19.什么是隧道***
		>***隧道是建立起来之后，就会在两条链接之间对原始数据进行盲转发的http应用程序
常见的用途是通过http连接承载加密的安全套接字层流量，这样ssl浏览就可以通过只允许web流量通过的防火墙了***
	- ***20.什么是agent代理***
		>***代表用户发起http请求的客户端程序***


--

### Updating . . .



