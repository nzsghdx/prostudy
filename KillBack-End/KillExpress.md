## Kill Express

***地址: [Bitbucket](https://bitbucket.org/nzsghdx/prostudy) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/tree/master) [GitHub](https://github.com/nzsghdx/ProStudy) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy)***

---

***本文目录***

- ***[简介]()***
- ***[HelloWorld]()***
- ***[应用生成器]()***
- ***[进阶]()***


### 简介

***Express 是基于 Node.js 平台，快速、开放、极简的 web 开发框架。***

### HelloWorld

- ***安装***
>***参考 [expressjs](http://www.expressjs.com.cn/starter/installing.html)***

	- ***依赖于Node,先下载安装好,环境变量什么的都搞好.***
	- ***创建一个空目录并在其内执行`npm install express --save`***
- ***测试***
	- ***在目录下创建个文件如`app.js`并输入如下代码:***

	```
	var express = require('express');
	var app = express();

	app.get('/', function (req, res) {
	  res.send('Hello World!');
	});

	var server = app.listen(3000, function () {
	  var host = server.address().address;
	  var port = server.address().port;

	  console.log('Example app listening at http://%s:%s', host, port);
	});
	```

	- ***执行`node app.js`***

- ***完成***  
***根据`app.js`里面写的端口号,浏览器访问如`http://localhost:3000/`测试是否输出如`app.js`里设置的那样的结果如`Hello World!`***

### 应用生成器
>***使用express用来生成应用骨架.参考 [express](http://www.expressjs.com.cn/starter/generator.html)***

- ***需要安装一下哈,如下命令***
***`npm install express-generator -g`***
>***我这样想的啊,上面HelloWorld安装的是express并不包括express-generator,现在安装.***

- ***在当前目录下生成一个项目如`myapp`:***
***`express myapp`***

- ***生成之后切入目录,键入命令安装所有依赖包:***
***`npm install`***

- ***windows下启动应用:***
***`set DEBUG=myapp & npm start`***

- ***浏览器访问是否成功创建项目:***
***`http://localhost:3000/`***

### 进阶

***Updating . . .***

