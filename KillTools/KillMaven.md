## Kill Maven

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillMaven.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillMaven.md?dir=0&filepath=KillMaven.md&oid=d98b02a3bb371d46107588aa9f8b833dce15701b&sha=cc28c9065b5245c6e2610cd78fbb9199dc2752b4) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillMaven.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy/src/cc28c9065b5245c6e2610cd78fbb9199dc2752b4/KillMaven.md?at=master&fileviewer=file-view-default)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

>感觉中文的Maven教程什么的不多,有的我还感觉乱七八糟的,以下是我日常使用时总结的一些技巧经验,部分参照了网络上其他人的经验.还有些是我认为很重要的在最下面,初学的时候容易搞不明白.

### Windows

***从[Maven官网](https://maven.apache.org/download.cgi)下载Maven,在Windows下需要如下配置两个环境变量.***

- ***M2_HOME:home目录***
- ***Path:home下的bin目录***

>检测是否安装成功,命令行下输入如下命令测试:

>
- ***`mvn -v`***
- ***`echo %M2_HOME%`***

### MyEclipse

>***MyEclipse中使用的话需要先配置一下,包括Maven和Jdk,在`widow/preferences/Myeclipse/Maven4Myeclipse`在更改Maven配置,在`widow/preferences/Java/Installed JREs`下更改Jdk配置.具体如下:***
>
- ***Myeclipse中`Maven4Myeclipse/installations`下更改自带Maven为本地手动下载的,避免bug.***
- ***Myeclipse中`Maven4Myeclipse/user settings`下更改配置文件为本地位置`browse...``path\maven\conf\settings.xml`,点击`update settings`然后`apply`.***
- ***如有需要可在`Installed JREs`下更改Jdk版本,Maven3好像需要Jdk1.7及以上才能支持.***

<center>***MyEclipse中运行项目的话需要右键项目,然后`run as`或`debug as`才能看到`mvn clean compile`等命令.或者找到存储在本地的文件夹,切到项目根目录使用命令行执行命令.***</center>

***MyEclipse导入Maven项目分为两种方式分别如下:***

- ***使用MyEclipse的普通工程导入，步骤如下：***
	- ***先打开dos，在项目根目录(mywebapp)下运行mvn eclipse:eclipse----运行完成后，会发现在工程目录下多了两个文件.classpath和.project,，这是eclise的工程文件，当然MyEclipse也识别。***
	- ***使用MyEclipse的导入功能，选择File-->Import...-->Existing Projects into workspace,选择工程目录，导入即可。***
	- ***关联Maven,导入的工程只是一个普通的工程，并不会与Maven插件相关联，此时在工程上点右键，Maven4MyEclipse--->Enable Dependency Management,这样这个项目就与Maven插件相关联了（关联后工程根节点左上角会出现一个M符号）***

 

- ***使用MyEclipse的Maven工程导入，步骤如下：***
	- ***使用MyEclipse的导入功能，选择File--->Import...-->Existing Maven Projects,选择工程目录，导入。这里导入，MyEclipse可能会联网更新索引，很慢。我们可以取消（在progress面板中点击红色按钮），这是工程导入进来了，但没有执行应有的操作，比如Maven的Dependency操作，此时进行一下第二步操作***
	- ***更新一下工程配置，右键工程-->Maven4MyEclipse-->Update Project Configuration。***


***Maven常用命令***

原来很多命令都是在命令行下完成的???

我一直搞错了,找解决方法也不知道怎么搜索,现在好了,记住几个常用命令好了,其实也很简单的,要经常用才好.

***实操***

***本地安装好Maven之后要怎么创建一个项目呢,Windows下使用命令行输入`mvn archetype:grentrate`按提示逐步操作.***

需要输入的简单参数解析如下:

- ***`groupid`:组名,一般是域名倒写,可以理解为'一系列'是一组.也是包名.***
- ***`artifactId`:一组中的某个,id是唯一的,用来标识这个唯一的项目.***
- ***`version`:开发的版本,例如QQ7.3,QQ8.4.***
- ***`packaging`:打包方式,可以是jar/war/zip等,默认值jar***
- ***`更多...`***

***或使用命令行输入`mvn archetype:grentrate -D`一次性生成全部.***

Java类需要先编译才能运行,一般IDE直接运行就是先自动编译然后运行的,Maven需要手动编译,编译运行命令如下:

- ***切到项目根目录`mvn compile`编译***
- ***执行编译命令之后,运行`mvn test`***

>***其他命令***
>
- ***`mvn clean`删除`target`目录,包括字节码和测试报告***
- ***`mvn package`打包命令,打包什么玩意暂时没搞明白***
- ***`mvn install`把自己的项目做成可复用的`.jar`包安装到本地仓库***

>***更多命令***
>
- 编译测试代码：mvn test-compile 
- 生成eclipse项目：mvn eclipse:eclipse  
- 生成idea项目：mvn idea:idea  
- 组合使用goal命令，如只打包不测试：mvn -Dtest package    
- 只打jar包: mvn jar:jar   
- 清除eclipse的一些系统设置:mvn eclipse:clean  
- mvn site                    生成项目相关信息的网站 
- mvn -Dwtpversion=1.0 eclipse:eclipse        生成Wtp插件的Web项目 
- mvn -Dwtpversion=1.0 eclipse:clean        清除Eclipse项目的配置信息(Web项目) 
- mvn package   生成target目录，编译、测试代码，生成测试报告，生成jar/war文件 
- mvn jetty:run     运行项目于jetty上
- mvn -e            显示详细错误 信息. 
- mvn validate        验证工程是否正确，所有需要的资源是否可用。 
- mvn integration-test     在集成测试可以运行的环境中处理和发布包。 
- mvn verify        运行任何检查，验证包是否有效且达到质量标准。     
- mvn generate-sources    产生应用需要的任何额外的源代码，如xdoclet。
- 仅打包Web页面文件 mvn war:exploded
- 打包时跳过测试 mvn package -Dmaven.test.skip=true
- [想要更多 那你很棒棒哦](https://www.google.com)


<center>***再往下就由接触阶段进入到熟悉阶段了***</center>

### `pom.xml`文件结构解析

>***pom.xml文件是干嘛的呢,Maven项目中最重要的就是这玩意了,各种配置信息都写在里面.***

```pom.xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <!-- 指定当前pom的版本 -->
  <modelVersion>4.0.0</modelVersion>

    <!-- 主项目坐标 -->
  <groupId>org.gpf.maventest</groupId> <!-- 域名倒置+项目名，也就是包名 -->
  <artifactId>maventest-demo</artifactId><!-- 项目名+模块名 -->
  <version>0.0.1-SNAPSHOT</version><!-- 版本号（3个数字） 大版本.分支版本.小版本snapshot(快照)|alpha（内测）|beta（公测）|Release（稳定）|GA（正式）-->
  <packaging>jar</packaging><!-- 打包方式，默认是jar，还可以是war、zip、pom -->

    <!-- 项目信息 -->
  <name>maventest-demo</name><!-- 项目描述名:显示给用户的名字(所谓对用户友好) -->
  <url>http://maven.apache.org</url><!-- 项目地址 -->
  <description></description><!-- 项目描述 -->
  <developers></developers><!-- 开发人员列表 -->
  <licenses></licenses><!-- 许可证信息 -->
  <organization></organization><!-- 组织信息 -->

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

    <!-- 依赖列表 -->
  <dependencies>
    <dependency>
      <groupId>junit</groupId><!-- 同上 -->
      <artifactId>junit</artifactId><!-- 同上 -->
      <version>4.10</version><!-- 所依赖的jar包具体版本 -->
      <type></type>
      <scope>test</scope><!-- 依赖范围，这里指定为test -->
      <optional></optional><!-- 依赖是否可选，默认是false -->
      <exclusions><!-- 排除依赖传递传递列表 -->
        <exclusion></exclusion>
      </exclusions>
    </dependency>
  </dependencies>

    <!-- 依赖管理,定义在父模块中供子模块继承使用 -->
  <dependencyManagement>
    <dependencies>
        <dependency></dependency>
    </dependencies>
  </dependencyManagement>

    <!-- 对构件提供支持 -->
  <build>
    <!-- 插件列表 -->
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>2.4</version>
        <executions>
            <execution>
                <phase>package</phase>
                <goals>
                    <goal>jar-no-fork</goal>
                </goals>
            </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

  <!-- 指定父模块 -->
  <parent></parent>
  <!-- 多模块编译 -->
  <modules>
    <module></module>
  </modules>
</project>
```

### 依赖范围(scope)

>***六种.是在`pom.xml`文件中的`scope`属性中配置依赖等限定作用范围时使用的.如\<scope>test\</scope>.***

- ***compile：缺省值，编译、测试都有效.***
- ***provided:编译，测试都有效，但是在运行时并不会加入。例如Servlet API，因为Web容器本身有，如果加入就会出现冲突.***
- ***runtime：测试、运行有效.***
- ***test：仅测试时有效.***
- ***system：与本机相关，可移植性差.***
- ***import：导入的依赖范围，仅适用在dependencyManager中，表示从其他pom导入dependency配置.***

### 依赖传递 
>***是啥呢,例如有3个maven项目，grandfather、father、son，其中father依赖grandfather，son又依赖father，他们之间构成依赖,构建项目时需要有依赖传递.咋弄呢,如下:***

***例如在Father项目的pom.xml中添加对GradFather的依赖，只需要在dependency标签中指定GrandFather的坐标即可.***

```
<dependencies>
    <dependency>
        <groupId>com.maventest</groupId>
        <artifactId>GrandFather</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
</dependencies>
```
***类似的在Son项目中添加对Father的依赖.那么现在构建Son就需要先构建GrandFather和Father.对GrandFather和Father进行`clean install`命令安装到本地仓库之后,son项目才能使用他们生成的jar包，然后再对Son项目进行`compile`命令即可,顺序一定要对,先`mvn clean install`再`mvn clean compile`.***

### 聚合与继承
>***如果项目要使用到GrandFather、Father、Son3个或其他多个模块时，就可以使用聚合.***

***新建一个项目如relationship,修改其`pom.xml`如下:***

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.maventest</groupId>
  <artifactId>relationship</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <!-- 注意package的值为pom,之前的都是jar/war等,这里不一样 -->
  <packaging>pom</packaging>

  <name>relationship</name>
  <url>http://maven.apache.org</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

<!-- 导入模块 -->
  <modules>
    <module>../GrandFather</module>
    <module>../Father</module>
    <module>../Son</module>
  </modules>
</project>```
```

***然后听说,在relationship项目运行`clean install`命令将会依次生成3个jar并加入本地仓库.***

>***如果在项目中每次都要配置某个依赖，我们可以向java编程那样将公共的模块抽取出来，这种方式叫做继承。新建一个maven项目如common，其`pom.xml`配置如下：***

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.maventest.common</groupId>
  <artifactId>common</artifactId>
  <version>0.0.1-SNAPSHOT</version>
	<!--这里属性值为pom-->
  <packaging>pom</packaging>

  <name>common</name>
  <url>http://maven.apache.org</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <junit-version>3.8.1</junit-version>
  </properties>

<dependencyManagement>
    <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>${junit-version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
</dependencyManagement>

</project>
```

***报错的话,删除src/test重新更新maven项目（右键项目-->Maven ForMyEclipse-->Update Project Configuration）.在子模块中可以这样使用公共`pom.xml`配置的依赖：***

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.maventest</groupId>
  <artifactId>GrandFather</artifactId>
  <version>0.0.2-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>GrandFather</name>
  <url>http://maven.apache.org</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

<!-- 引入父模块坐标 -->
<parent>
    <groupId>com.maventest.common</groupId>
    <artifactId>common</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</parent>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
    </dependency>
  </dependencies>
</project>
```

### 常见错误

>***待续. . .***





***其他:***

- ***在我看一本讲Maven的书的时候,里面有个词`构件`,让我很费解啊,他也不解释,后来知道了意思是`maven中任何一个依赖、插件都可以被称为构件.`***
- ***`依赖`在Maven中我认为是个很重要的概念,Maven有个显著特点就是可以根据`pom.xml`中配置的依赖信息自动下载所需的`jar`包.***
- ***`仓库`是用来存储`jar`包的,分为[远程仓库](http://repo1.maven.org/maven2/)/[依赖查询](http://mvnrepository.com/)等和本地仓库,本地仓库的存在避免了每次执行项目的时候都需要重新下载,远程仓库可确定依赖jar包的具体版本.***
- ***`坐标`是仓库用来管理jar包的.`pom.xml`中的一条条配置信息就叫坐标.所有构件通过坐标作为其唯一标识.***
- ***Maven是基于插件的,可以在[Maven官网](http://maven.apache.org/plugins/index.html)找到丰富的合适的插件.***
- ***更改`maven/conf/settings.xml`文件中的`<localRepository>`属性可改变本地仓库在本地的存储位置.移到注释范围外更改才有效.***
- ***更多正在更新中 . . .***



### 命令
- ***`mvn install`将自己项目已经打包好的jar包安装到你的本地仓库中***  
- ***`mvn package`将代码打包到输出目录，一般在target下面***

