## Kill Git

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillGit.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillGit.md?dir=0&filepath=KillGit.md&oid=32d1eb60ecc5bb585698096431465d7cb00f162e&sha=3e7efb7dbedb79b75133b8f09367a57795a28dee) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillGit.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy/src/3e7efb7dbedb79b75133b8f09367a57795a28dee/KillGit.md?at=master&fileviewer=file-view-default)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

---

***参照了[廖雪峰Git](http://www.liaoxuefeng.com)***

### Git 版本控制系统

***下载地址:百度一下不就好了吗:[官方网站]()***

***安装之后配置第一步:自报家门***

`git config --global user.name ""`

`git config --global user.email ""`

***让git显示颜色[可选]***

`git config --global color.ui true`

***为命令设置别名[随意]***

`git config --global alias.st status`

***删除别名目录***

`git/config`

---

***简单实操如下:四步***

- ***创建版本库 仓库 repository***

```
mkdir learngit
cd learngit 
pwd
...
```

- ***设置git可管理***

	- ***1.切换到创建好的仓库目录内部***
	- ***2.执行初始化仓库`git init`命令***


- ***把文件放到git仓库***

	- ***1.首先把文件放到git仓库的文件夹***
	- ***2.`git add readme.txt`***
	- ***3.`git commit -m "本次提交说明"`***

- ***设置远程库***
	- ***0.创建远程库之前或之后还需要添加公钥,为了识别是谁推送的代码.可使用`ssh-keygen -t rsa -C "youremail@example.com"`命令之后在`/user/.ssh`目录下如`C:\Users\nzsghdx\.ssh`找到`id_rsa.pub`文件打开复制到远端添加SSH公钥.***
	- ***1.先在远端创建一个仓库,本人常用有 [GitHub](https://github.com/nzsghdx/) [GitOsc](http://git.oschina.net/nzsghdx/) [Coding](https://coding.net/u/nzsghdx/) [BitBucket](https://bitbucket.org/nzsghdx/)***
	- ***2.运行命令进行关联 `git remote add origin git@github.com:michaelliao/learngit.git`***
	- ***3.推送到远程库 `git push -u origin master`***
	- ***4.下面还有个 [实战操作](#szcz).***

>- ***`youremail@example.com`是你注册要推送的远程库的邮箱地址.***
- ***测试是否关联远程库成功使用`ssh -T git@github.com`命令.根据远程库的不同可改变`git@`后面的值.***
- ***GitHub和BitBucket是国外的服务,GitHub应用最广范,GitOsc(开源中国旗下)和Coding是国内的服务,国内一般推送代码较快,具体区别及优劣可通过搜索引擎查看.***
- ***origin是远程库的名字,可以自定义为你想要的名字`git@`后面的内容为远程库地址,例如上面`github.com`是托管服务商网址,`michaelliao`是你的用户名,`learngit`是你在远端创建的仓库名字,`.git`别忘了加上,应该是代表仓库类型吧.***
- ***推送到远程库之前可先使用`git pull origin master`拉取一下代码.有冲突则解决冲突 [解决冲突在此](#jjct)***
- ***第一次推送远程库如果是空的话的时候可加上`-u`参数,可把本地master分支和远程master分支管理拿起来,以后可通过简化命令``直接推送到指定分支.***

### 其他命令

***更改commit后的描述信息[看情况]***

- `git commit --amend`


- ***查看状态***

	`git status`

***查看被修改的内容***

- `git diff`

***查看历史记录/从近到远显示全部提交历史***

- ***`git log`***

***单行显示*** 

- ***`git log --pretty=oneline`***

***版本回退//HEAD指向的版本就是当前版本***

- ***`git reset --hard HEAD^/--HEAD~100`***

- ***`git reset --hard commit id`***

***每一次执行命令***

- ***`git reflog`***

***工作区***

- ***工作目录//文件夹文件目录***

***版本库***

- ***.git虽然在工作区中,但是他是git的版本库***

***暂存区//git独有/貌似是跟svn相比***

- ***`git add` 是把文件添加到暂存区***

- ***`git commit` 是把暂存区的所有内容提交到当前分支***

***查看工作区和暂存区的区别***

- ***`git diff HEAD --readme.txt`***

***撤销修改***

- ***`git checkout -- readme.txt`***

***撤销到最近一次git add / git commit 的位置***

- ***修改没被放到暂存区的时候,撤销修改***

- ***修改被添加到暂存区之后,撤销添加到暂存区后进行的再次修改***

***撤销添加到暂存区的修改***

- ***`git reset HEAD readme.txt`***

***删除文件***

- ***从工作区删除 `rm file`***
- ***从版本库删除 `git rm file`***
- ***从版本库恢复 `git checkout -- file`***

***GitHub***

- ***1.先在远端创建一个仓库***
- ***2.运行命令进行关联 `git remote add origin git@github.com:michaelliao/learngit.git`***
- ***3.推送到远程库 `git push -u origin master / git push origin master`***

***可以把一个本地git仓库关联到多个远程仓库(repository)***

- ***`git remote add originName git@repo:userName/gitrepo.git`***
- ***每次添加远程哭时只需保证originName不同就好***

***克隆本地库***

- ***`git clone git@github.com:michaelliao/gitskills.git`***
- ***git支持多种格式,包括https,但是通过ssh支持的原生的git协议速度最快***

***创建并切换到dev分支 -b/切换***

- ***`git checkout -b dev`***

***创建分支***

- ***`git branch dev`***

***查看所有分支 //分支里面没有内容的话，将找不到分支。。***

- ***`git branch`***


***切换分支***

- ***`git checkout master`***

***合并分支***

- ***`git merge dev`***

***删除分支***

- ***`git branch -d dev`***

***强制删除***

- ***`git branch -D dev`***

***查看分支合并图***

- ***`git log --graph`***

>***合并有冲突的时候,修改冲突文件提交的时候 就是合并成功的时候***

***合并参数***

- ***非快速合并 `--no-ff`***
- ***普通模式合并后的历史有分支,能看出来曾经做过合并***

***bug分支***

- ***1.当当前分支没有提交的时候,可以先把工作区隐藏起来 `git stash`***
- ***2.修复bug之后切换到当前分支,使用`git stash pop`继续工作***

***多人协作/<span id = 'jjct'>解决冲突</span>***

- ***1.`git push origin branch-name`***
- ***2.如果推送失败,`git pull`***
- ***3.合并***
- ***4.`git push origin branch-name`***
- ***5.`git branch -set-upstream branch-name origin/branch-name`***

***查看远程库信息***

- ***`git remote -v`***

***推送分支***

- ***`git push origin branch-name`***

***在本地创建和远程分支对应的分支***

- ***`git checkout -b branch-name origin/branch-name`***

***建立本地分支和远程分支的关联***

- ***`git branch --set-upstream branch-name origin/branch-name`***

***创建标签***

- ***`git tag name`***
- ***`git tag -a tagname -m 'info'`***
- ***`git tag -s tagname -m 'info'`***

***查看所有标签***

- ***`git tag`***

***展示某个标签***

- ***`git show tagname`***

***删除标签***

- ***本地删除`git tag -d tagname`***
- ***远端删除`git push origin :refs/tags/v0.9`***

***推送标签***

- ***`git push origin tagname`***
- ***`git push origin --tags`***

***忽略配置文件//放到版本库中,并且可以对.gitignore***

- ***`.gitignore`***


***<span id = 'szcz'>实战操作</span>***

***创建好远程仓库之后***

- ***设置sshkey,设置好sshkey之后和远程的关联起来***
	***查看是否关联成功***
		***`ssh -T git@github.com`***
- ***设置好全局用户名和邮箱地址***
	- ***`git config --global user.name ""`***
	- ***`git config --global user.email ""`***
- ***关联远程仓库和本地仓库***
	- ***1.首先要创建个和远程仓库同名的本地仓库***
	- ***2.`git remote origin git@github.com:nzsghdx/learngit.git`***
	- ***3.首先拉一下远程仓库 `git pull origin master`***
	- ***4.查看不同之后推送仓库 `git push origin master`***
- ***克隆***
	- ***`git clone git@github.com:nzsghdx/learngit.git`***

---

***Question***

- ***git clone和git pull区别***



***其他***

- ***microsoft的word格式是二进制格式,所以版本控制系统不能跟踪word文件的改动***
- ***统一编码格式***



***有什么问题可以通过搜索引擎解决或找我交流,联系方式在README.md中.***

















