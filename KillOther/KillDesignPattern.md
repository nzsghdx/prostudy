## Kill DesignModel

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillGit.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillGit.md?dir=0&filepath=KillGit.md&oid=32d1eb60ecc5bb585698096431465d7cb00f162e&sha=3e7efb7dbedb79b75133b8f09367a57795a28dee) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillGit.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy/src/3e7efb7dbedb79b75133b8f09367a57795a28dee/KillGit.md?at=master&fileviewer=file-view-default)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

---

***参照了百度里某个不知转载了多少遍的博客.***

### 设计模式

- ***在软件开发周期中每一个阶段都存在着一些被认同的模式***  
- ***软件模式是在软件开发过程中某些可重现问题的有效解决方法***  
	- ***软件模式的基础结构主要由四部分构成 问题描述 前提条件 解法 效果***  

- ***软件模式与具体的领域无关,无论做什么开发都可以使用软件模式***
- ***在软件模式中设计模式是研究最为深入的模式***
- ***设计模式用于在特定的条件下为一些重复出现的软件设计问题提供合理的有效的解决方案***

- ***设计模式是面向对象软件工程的一个重要研究分支***
	- ***无论是在大型api或框架,轻量级框架,还是在应用软件的开发中,设计模式都得到了广泛的应用***

- ***使用设计模式可以说是站在巨人的肩膀上***
	- ***通过一些成熟的方案来指导新项目的开发和设计,以便于开发出具有更好的灵活性和扩展性,也更易于复用的软件系统***

- ***设计模式:是一套被反复使用多数人知晓的,经过分类编目的,代码设计经验的总结,使用设计模式是为了可重用代码***
	- ***让代码更容易被他人理解并且保证代码可靠性***
	- ***广义来说:就是代码设计经验的总结***
	- ***狭义来说:23中设计模式,但是设计模式并不是只有这23种***

- ***设计模式一般包含模式名称,问题,目的,解决方案,效果等组成元素***
	- ***其中关键要素是模式名称,问题,解决方案和效果***
	- ***模式名称通过一两个词来描述模式的问题,解决方案和效果,以便更好的理解模式并方便开发人员之间的交流***  

		>***绝大多数模式都是通过其功能或模式结构来命名的***
	- ***问题描述了应该在何时使用模式,它包含了设计中存在的问题以及问题存在的原因***
	- ***解决方案描述了一个设计模式的组成部分,以及这些组成部分之间的相互关系,各自的职责和协作方式***
	- ***效果描述了模式的优缺点以及在使用设计模式时应考虑的问题***

- ***23三种设计模式按照用途可分为 创建型 结构型 行为型***
	- ***创建型:主要用于描述如何创建对象 5***
		- ***创建型模式:单例模式 简单工厂模式 工厂模式方法 抽象工厂模式 原型模式 建造者模式***
	- ***结构模式:主要用于描述如何实现类或对象的组合 7***
		- ***结构型模式:适配器模式 桥接模式 组合模式 装饰模式 外观模式 享元模式 代理模式***
	- ***行为型:主要用于描述类或对象怎样交互以及怎么样分配职责 11***
		- ***行为型模式:职责链模式 命令模式 解释器模式 迭代器模式 中介者模式 备忘录模式 观察者模式 状态模式 策略模式 模板方法模式访问者模式***


- ***按照主要用于处理类之间的关系还是主要处理对象之间的关系分为 类模式和对象模式***
	- ***单例模式是对象创建型模式***
	- ***模板方法是类行为型模式***


***个人认为常见常用:单例模式 简单工厂模式 工厂模式方法 适配器模式 代理模式 观察者模式***

- ***设计模式的作用***
	- ***1.设计模式来源于开发者的经验和智慧,是从许多优秀的软件系统中总结出来的成功的,能够实现可维护复用的设计方案***
例如使用设计模式可以避免做一些重复性的工作***
	- ***2.设计模式提供一套通用的设计词汇和一种通用的形式来方便开发人员之间的沟通和交流,使得设计方案更加通俗易懂***
	- ***3.大部分世纪模式都兼顾了系统的可复用性和可扩展性,可以更好的复用已有的设计方案功能模块甚至一个完整的软件系统***
	- ***4.合理使用设计模式并对设计模式的使用情况进行文档化,将有助于别人更快的理解系统***
	- ***5.设计模式将有助于初学者更加深入的理解面向对象思想***

- ***设计模式概念***
	- ***比较官方一点的说法就是 一套被反复复用的,多数人知晓的,经过分类编目的,代码设计经验的总结***
	- ***设计模式是软件设计的思想***

- ***设计模式的作用***
	- ***设计模式就是为了提高代码的复用性,可以让代码更容易被其他人理解,可以保证代码的可靠性***

- ***设计模式的六大原则***
	- ***1.开闭原则 : 对扩展开放对修改关闭,实现一个热插拔的原则,就要用到接口和抽象类***
	- ***2.里氏代换lsp原则 : 是面向对象的基本原则之一,任何基类可以出现的地方子类一定可以出现,是继承复用的基石 ,是对开闭原则的补充***
	- ***3.依赖倒转原则 : 开闭原则的基础 针对接口编程 依赖于抽象而不依赖于具体***
	- ***4.接口隔离原则 : 使用多个隔离的接口比使用一个单独的接口要好,可以降低类之间的耦合度***
	- ***5.迪米特原则 : 最少知道原则 一个实体应当尽量少的与其他实体之间发生相互作用,使系统的功能模块相对独立***
	- ***6.合成复用原则 : 尽量使用合成/聚合的方式,而不是使用继承***

- ***工厂模式分为两种***
	- ***普通工厂模式 建立一个工厂类,对实现了同一接口的一些类实例的创建***

		```
{
	发送邮件和短信的栗子
	public interface Sender(){
		pbulic void send();
	}
	实现类
	pbulic class mail impl sender{
		@override
		public void send(){
			syso("this is mail");
		}
	}
	public class sm impl sender{
		@override
		public void send(){
			syso("this is sm");
		}
	}
	工厂类
	public class sendf{
		public sender produce(String type){
			if("mail".equalls(type)){
				return new mail();
			}else if("sm".equalls(type)){
				return new sm();
			}else{
				syso("输入正确的类型");
				return null;
			}
		}
	}
	test
	public static void main(){
		sendf f = new sendf();
		sender s = f.produce("sm");
		s.send();
	}
}
		```

		***多个工厂方法模式***

		```
{
	***工厂类中多个方法***
	***创建不同的实例***
}
		```

		***静态方法模式***

		```
{
	***把工厂类中的方法设置为静态的就好***
	***不需要声明直接调用***
}
		```  


	- ***抽象工厂模式***
		- ***实现类实现同一个接口***
		- ***工厂类实现同一个接口,重写同样的方法***
		- ***工厂模式,类的创建依赖工厂类***
		- ***如果要扩展程序就需要对工厂类修改,这违背了开闭原则***
		- ***创建多个工厂类,这样一旦增加新的功能直接增加新的工厂类就好了***


		```
{
	发送邮件和短信的栗子
	public interface Sender(){
		pbulic void send();
	}
	实现类
	pbulic class mail impl sender{
		@override
		public void send(){
			syso("this is mail");
		}
	}
	public class sm impl sender{
		@override
		public void send(){
			syso("this is sm");
		}
	}
	两个工厂类
	public class mailf impl provider{
		@override
		public sender produce(){
			return new mails();
		}
	}
	public class smf impl provider{
		@override
		public sender produce(){
			return new sms();
		}
	}
	interface
	public interface provider{
		public sender produce();
	}
	test 
	public static void main(){
		provider pv = new mailf();
		sender s = pv.produce();
		s.send();
	}
}
		```

- ***单例模式是一种常见的设计模式***

	>***保证在Java虚拟机中只有一个实例对象***

	- ***优点是:***
		- ***1.某些类创建比较繁琐,是一笔很大的系统开销,占用系统资源***
		- ***2.省去了new操作符,减低了系统内存的使用频率,减轻垃圾回收的压力***
		- ***3.控制交易流程的话,如果类创建多个的话,系统就会混乱了***

		***懒汉模式***

		```
{
	private static singleton instance = null;
	private singleton(){}
	public static singleton getinstance(){
		if(instance ==null){
			instace = new singleton();
		}
		return isntance;
	}
}
		```

		***饿汉模式***

		```
{
	private static singleton instance = new instance();
	private singleton(){}
	public sync static singleton(){
		return instance;
	}
}
		```

- ***适配器模式***
	- ***适配器模式 重点是适配 将某个类的接口转换为客户端期望的另一个接口表示***
	- ***目的是为了消除接口不匹配所造成的兼容性问题***
	- ***类的适配器 对象的适配器 接口的适配器***

	```
{
	类的适配器
	有一个source类中的方法待适配,目标接口是targettable,通过adapter类,将source的功能扩展到targettable中
	public class source{
		public void method1(){
			syso("this is original method");
		}
	}
	public interface targeettalbe{
		public void method1();
		public void method2();
	}
	public class adapter extends source impl targettable{
		@override
		public void method2(){
			syso("this is targettable method");
		}
	}
	test
	targettable t = new adapter();
	t.method1();
	t.method2();
}
	```

- ***装饰模式***
	- ***给对象增加一个动态的新的功能,要求装饰对象和被装饰对象实现同一个接口***
	- ***装饰对象持有被装饰对象的实例***

	```
{
	public interface sourceable(){
		public void method();
	}
	public class source impl sorceable(){
		@override
		public void method(){
			syso("the original method"):
		}
	}
	public class decorator impl sourceable{
		private sourceable sa;
		public decorator(sourceable sa){
			super();
			this.sa=sa;
		}
		@override
		public void method(){
			syso("before decorator");
			sa.method();
			syso("after decorator");
		}
	}
	test
	public class decorator{
		sourceable s = new source();
		sorceable obj = new decorator(s):
		obj.method();
	}
}
	```

- ***代理模式***
	- ***使用代理模式可以将功能划分的更加清晰,有助于后期维护***
	- ***代理模式就是多出一个代理类,替原对象进行一些操作***

	```
{
	public interface sourceable{
		public void method();
	}
	public class source impl sourceable{
		@override
		public void method(){
			syso("the original method");
		}
	}
	public class proxy impl sorceable{
		private source s;
		public proxy(){
			super();
			this.s=new source();
		}
		@override
		public void method(){
			before();
			s.method();
			after();
		}
		private void after(){
			syso("after");
		}
		private void before(){
			syso("before"):
		}
	}
	test
	sourceable s = new proxy();
	s.method();
}
	```

- ***观察者模式***
	- ***当一个对象变化时,其他依赖该对象的对象都会收到通知,并且随着变化***
	- ***是一种一对多的关系***

	```
{
	public interface observer{
		public void update();
	}
	public class ob1{
		@override
		public void update(){
			syso("ob1");
		}
	}
	public class ob2{
		@override
		public void update(){
			syso("ob2");
		}
	}
	public interface subject{
		public void add(observer obs);
		public void del(observer obs);
		public void notifyobservers();
		public void operation();
	}
}
	```


--

### Updating . . .

