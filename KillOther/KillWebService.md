## Kill WebService

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/blob/master/KillWebService.md) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/blob/master/KillMaven.md?dir=0&filepath=KillMaven.md&oid=d98b02a3bb371d46107588aa9f8b833dce15701b&sha=cc28c9065b5245c6e2610cd78fbb9199dc2752b4) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/git/blob/master/KillService.md) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy/src/cc28c9065b5245c6e2610cd78fbb9199dc2752b4/KillMaven.md?at=master&fileviewer=file-view-default)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

***那么先了解一下基本概念好了,知道是什么然后实战.***

***Web Service可以将应用程序转换为网络应用程序,通过Web Services可以向全世界发布信息,或提供某项功能***

***Web Services可以被其他应用程序使用***

- ***基本的Web Services平台是xml+http***
- ***Web Services使用xml来编解码数据,并使用soap来传输数据***
- ***Web Services可使您的应用程序成为web应用程序***
- ***Web Services通过Web进行发布,查找和使用***

- ***什么是Web Services***
	- ***Web Services是应用程序组件***
	- ***Web services使用开放协议进行通信***
	- ***web services是独立的(self-contained)并可自我描述***
	- ***web services可通过使用uddi来发现***
	- ***web services可被其他应用程序使用***
	- ***xml是web services的基础***
- ***Web Services如何工作***
	- ***基础的web services平台是xml+http***
	- ***http协议是最常用的因特网协议***
	- ***xml提供了一种可用于不同平台和编程语言之间的语言***
- ***Web Services平台的元素***
	- ***soap 简单对象访问协议***
	- ***uddi 通用描述,发现及整合***
	- ***wsdl web services描述语言***
- ***Web Services平台是简单的可共同操作的消息收发框架***
- ***Web Services的作用***
	- ***所有的平台都可以通过web浏览器来访问web,不同的平台之间可以借此交互(不同的操作系统可以通过浏览器访问相同的页面),所以诞生了Web Services***
    - ***Web应用程序是运行在web上的简单应用程序,他们围绕web浏览器标准被进行构建,几乎可被任何平台上的任何浏览器来使用***
- ***Web Services把Web应用程序提升到了另外一个层面***
	- ***通过web services,您的应用程序可向全世界发布功能或消息***
	- ***Web Services使用xml来编解码数据,并使用soap借由开放的协议来传输数据***
- ***web services有两种类型的应用***
	- ***可重复使用的应用程序组件***
		- ***web services可以把应用程序组件作为服务来提供,比如汇率转换/天气预报/语言翻译***
	- ***连接现有的软件***
		- ***通过为不同的应用程序提供一种链接起数据的途径,web services有助于解决协同工作的问题***
		- ***通过使用web services,您可以在不同的应用程序与平台之间来交换数据***
- ***web services拥有三种基本元素***
	- ***soap,wsdl,uddi***
- ***什么是soap***
	- ***基本的web services平台是xml+http***
	- ***soap指简单对象访问协议***
	- ***soap是一种通信协议***
	- ***soap用于应用程序之间的通信***
	- ***soap是一种用于发送消息的格式***
	- ***soap被设计用来通过因特网进行通信***
	- ***soap独立于平台***
	- ***soap独立于语言***
	- ***soap独立于xml***
	- ***soap很简单并可扩展***
	- ***soap允许您绕过防火墙***
	- ***soap将作为w3c标准来发展***
- ***什么是wsdl***
	- ***wsdl是基于xml的用于描述web services以及如何访问web services的语言***
	- ***wsdl指网络服务描述语言***
	- ***wsdl使用xml编写***
	- ***wsdl是一种xml文档***
	- ***wsdl用于描述网络服务***
	- ***wsdl也可用于定位网络服务***
	- ***wsdl还不是w3c标准***
- ***什么是uddi***
	- ***uddi是一种目录服务,通过它,企业可注册并搜索web services***
	- ***uddi指通用的描述,发现以及整合(universal description,discovery and integration)***
	- ***uddi是一种用于存储有关web services的信息的目录***
	- ***uddi是一种由wsdl描述的网络服务接口目录***
	- ***uddi经由soap进行通讯***
	- ***uddi被构建与microsoft.net平台之中***
- ***那么理解一下soap,wsdl,uddi***
	- ***soap定义协议,wsdl定义soap怎么访问web service,uddi用来存储web services的信息目录等***
- ***任何应用程序都可拥有web services组件,web services的创建与编程语言的种类无关***
- ***wsdl*** 
	- ***wsdl是基于xml的用来描述web services以及如何访问他们的一种语言***
	- ***wsdl可描述web services,连同用于web services的消息格式和协议细节***
- ***soap***
	- ***soap是一种使应用程序有能力通过http交换信息的基于xml的简单协议***
	- ***soap是一种用于访问web services的协议***

***上面是我在某个摘抄的,下面是另外一个地方摘抄的,结合起来看看就好,重点是实战.更能加深理解.***


- ***web services简介***
	- ***web services是自包含,自描述的模块化应用,能在网络上发布,定位和调用***
	- ***自包含:客户端,编程语言只要支持xml和http客户端即可,无需额外软件.服务端需要一个web服务器和servlet引擎.客户端和服务端能在不同的环境中实现,有可能不需要写代码就可以将一个已存在的 应用转换为web服务***
	- ***自描述:客户端和服务端仅需识别请求/响应消息的格式和内容.消息本身携带消息格式定义,不需要外部元数据或代码生成工具***
	- ***模块化:简单的web服务可通过工作流技术或调用更底层web服务聚合成更复杂的web服务***
	- ***平台无关性:web服务基于一系列开放的,基于xml的标准,这些标准可作为一个web服务和各种计算平台上,编程语言的客户端提供互用性***
	- ***web services是一种分布式软件体系结构,有各种规范,协议,有不同的实现和开发工具支持,例如在jee和.net上都有实现***
	- ***满足一定的规范如ws-i后,web services间可实现互操作,例如一个web services调用另外一个web services***
		
- ***web services的基本协议有***
	- ***soap:simple object access protocol,简单对象访问协议.soap属于消息传递协议,基于xml,他对消息进行了编码,这样就可以通过传输协议(http,hop,smtp...)在网络上传递他们***
	- ***wsdl:web services description,web服务描述语言,wsdl属于接口描述协议,基于xml,描述了网络上web服务的接口和实例.wsdl文档可通过uddi,wsil获得,或者通过email,网站等公开其url***
	- ***ws-security:web服务安全性规范,为安全通信定义了基于令牌的体系结构***
	- ***uddi:universal description discovery and integraton,统一描述,发现和集成协议,为查找和访问服务定义了注册中心和相关协议***
	- ***wsil:web services inspection language,web服务检查语言.wsil基于xml,定义了分布式的服务发现方法,是对uddi的补充,可在web站点上发现服务***

- ***web services可分为三类:***
	- ***1.业务信息:企业与消费者或者企业共享信息,例如天气预报/股票行情***
	- ***2.业务集成:企业为消费者提供事务性的收费服务,例如订票系统,电子商务***
	- ***3.业务工程整合:企业与其合作伙伴如制造商,组装商等整合业务***
			
- ***web services中有三种角色:***
	- ***1.服务提供者(service provider),创建,部署和发布web服务***
	- ***2.服务代理(service broker),注册,分类被发布的web服务,并提供服务查询,例如uddi可作为wsdl描述的web服务的服务代理***
	- ***3.服务客户端(service client),使用代理服务发现,绑定和调用服务提供者***

- ***service broker ←discover→ service requestor***
- ***service provider  ←request/response→ service requestor***
- ***service provider publish→ service broker***
