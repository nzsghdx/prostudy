## Kill Linux

>***Updating...***

***地址:[GitHub](https://github.com/nzsghdx/ProStudy/) [GitOsc](http://git.oschina.net/nzsghdx/ProStudy/) [Coding](https://coding.net/u/nzsghdx/p/ProStudyPub/) [Bitbucket](https://bitbucket.org/nzsghdx/prostudy)***


>***Nobody can hear you. Nobody cares about you. Nothing will come of this.***

---

***暂收录想起来的常用命令...***

- ***本文目录:***
	- ***[常用命令](#cyml)***
	- ***[目录结构](#mulu)***
	- ***[更换安装源](#yuan)***
	- ***[查看版本](#banben)***
	- ***[Vim简单实用](#vim)***
	- ***[常用命令](#cyml)***
	- ***[Iptables](#cyml)***
	- ***[RAM/SWAP](#cyml)***
	- ***[每天一个linux命令](#linux)***

### <span id = 'cyml'>linux常用命令</span>

- ***`ls` 查看列表***
	- ***`ls -a` 查看所有，包括隐藏文件 `a=all`***
	- ***`ls -l (ll)` 以列表显示 `l=list`***
	- ***`ls -al`***

- ***`cd` 切换目录***
	- ***`cd ./path` 切换到当前目录下的某个目录***
	- ***`cd /path` 切换到根目录（/）下的某个目录***
	- ***`cd ..` 切换到上一层目录***
	- ***`cd ~` 切换到/home目录下***

- ***`rm` 删除文件***
	- ***`rm -r` 递归删除（删除一个目录）***
	- ***`rm -f` 强制删除***
	- ***`rm -rf`***

- ***`cat` 查看某个文件***
	- ***`cat path`***

- ***`mkdir` 创建目录***

- ***`touch` 创建文件***

- ***`vi` 打开`vim`编辑器***
	- ***`vi filename` 使用vim编辑器打开某个文件***
	- ***`：w` 保存***
	- ***`：w！`强制保存***
	- ***`：q` 退出***
	- ***`：q！`强制退出***
	- ***`：wq` 保存退出***
	- ***`：wq！`保存后强制退出***

- ***`du` 查看硬盘占用 `desk use`***
	- ***`du -h` 人性化输出硬盘占用信息 h=human***
	- ***`df`***
	- ***`df -h`***

- ***`ifconfig` 查看配置信息***
	- ***`iftop` 查看端口/网络***

- ***`apt-get install update` 更新系统***
	- ***`apt-get install upgrade` 升级系统***
	- ***`apt-get install filename` 安装软件***

- ***`wget path` 下载文件***

- ***`unzip` 解压`zip`文件***

- ***`cp` 复制文件***
	- ***`cp path/filename1 path/filename2` 复制文件到某个路径***

- ***`username -a` 查看系统信息***

- ***`su` 切换到root用户***

- ***`shutdown -r -t 0` 立即重启`t=time`***
	- ***`reboot` 重启***

- ***`linux`输入密码的时候不显现***
	- ***`tab`键可以快速补全命令***


---

### <span id="mulu">目录结构</span>
>***一些常用的目录和目录保存的内容***

```
/usr/share/    工具安装目录

/usr/share/images/desktop-base   壁纸

/usr/share/xfce4/backdrops/  壁纸

/usr/share/themes/ 主题文件 

/usr/share/icons/  鼠标主题

/usr/share/applications/ 系统图标

/usr/share/pixmaps/ 图标
```

---

### <span id="yuan">更换安装源</span>
>***因为linux是洋人搞出来的玩意,且由于某些原因使用原来的安装源的话更新速度很慢,可更改为国内镜像站地址.***

***两种方式***
>***更改之后最好update一下:`sudo apt-get update`***

- ***编辑文件***
	- ***配置安装源的文件在`/etc/apt/sources.list`,使用命令打开并编辑如下,以debian为例,更多就在 [脑子是个好东西](www.nzsghdx.com) 或 [文件]()***

	```
	添加以下163源：
	#####################主要，开源，闭源
	deb http://mirrors.163.com/debian wheezy main non-free contrib
	deb-src http://mirrors.163.com/debian wheezy main non-free contrib

	###################wheezy-proposed-updates建议更新
	deb http://mirrors.163.com/debian wheezy-proposed-updates main contrib non-free
	deb-src http://mirrors.163.com/debian wheezy-proposed-updates main contrib non-free

	#wheezy-updates推荐更新
	deb http://mirrors.163.com/debian wheezy-updates main contrib non-free
	deb-src http://mirrors.163.com/debian wheezy-updates main contrib non-free


	#wheezy/updates安全更新
	deb http://mirrors.163.com/debian-security wheezy/updates main contrib non-free 
	deb-src http://mirrors.163.com/debian-security wheezy/updates main contrib non-free 

	deb http://http.us.debian.org/debian wheezy main contrib non-free
	deb http://security.debian.org wheezy/updates main contrib non-free


	#上海交大
	deb http://ftp.sjtu.edu.cn/debian wheezy main non-free contrib
	deb http://ftp.sjtu.edu.cn/debian wheezy-proposed-updates main non-free contrib
	deb http://ftp.sjtu.edu.cn/debian-security wheezy/updates main non-free contrib
	deb-src http://ftp.sjtu.edu.cn/debian wheezy main non-free contrib
	deb-src http://ftp.sjtu.edu.cn/debian wheezy-proposed-updates main non-free contrib
	deb-src http://ftp.sjtu.edu.cn/debian-security wheezy/updates main non-free contrib

	#上海交大 ipv6
	deb http://ftp6.sjtu.edu.cn/debian wheezy main non-free contrib
	deb http://ftp6.sjtu.edu.cn/debian wheezy-proposed-updates main non-free contrib
	deb http://ftp6.sjtu.edu.cn/debian-security wheezy/updates main non-free contrib
	deb-src http://ftp6.sjtu.edu.cn/debian wheezy main non-free contrib
	deb-src http://ftp6.sjtu.edu.cn/debian wheezy-proposed-updates main non-free contrib
	deb-src http://ftp6.sjtu.edu.cn/debian-security wheezy/updates main non-free contrib

	#网易 
deb http://mirrors.163.com/debian/ wheezy main non-free contrib
deb http://mirrors.163.com/debian/ wheezy-proposed-updates main non-free contrib
deb-src http://mirrors.163.com/debian/ wheezy main non-free contrib
deb-src http://mirrors.163.com/debian/ wheezy-proposed-updates main non-free contrib

	#搜狐
deb http://mirrors.sohu.com/debian/ wheezy main non-free contrib
deb http://mirrors.sohu.com/debian/ wheezy-proposed-updates main non-free contrib
deb-src http://mirrors.sohu.com/debian/ wheezy main non-free contrib
deb-src http://mirrors.sohu.com/debian/ wheezy-proposed-updates main non-free contrib

	#中国科技大学
deb http://mirrors.ustc.edu.cn/debian/ wheezy main non-free contrib
deb http://mirrors.ustc.edu.cn/debian/ wheezy-proposed-updates main non-free contrib
deb-src http://mirrors.ustc.edu.cn/debian/ wheezy main non-free contrib
deb-src http://mirrors.ustc.edu.cn/debian/ wheezy-proposed-updates main non-free contrib

	#清华大学 不是很稳定
deb http://mirrors.tuna.tsinghua.edu.cn/debian/ wheezy main non-free contrib
deb http://mirrors.tuna.tsinghua.edu.cn/ wheezy-proposed-updates main non-free contrib
deb-src http://mirrors.tuna.tsinghua.edu.cn/ wheezy main non-free contrib
deb-src http://mirrors.tuna.tsinghua.edu.cn/ wheezy-proposed-updates main non-free contrib

	#北京理工
deb http://mirrors.bit.edu.cn/debian/ wheezy main non-free contrib
deb http://mirrors.bit.edu.cn/ wheezy-proposed-updates main non-free contrib
deb-src http://mirrors.bit.edu.cn/ wheezy main non-free contrib
deb-src http://mirrors.bit.edu.cn/ wheezy-proposed-updates main non-free contrib

	#北京理工（Ipv6）
deb http://mirrors.bit6.edu.cn/debian/ wheezy main non-free contrib
deb http://mirrors.bit6.edu.cn/ wheezy-proposed-updates main non-free contrib
deb-src http://mirrors.bit6.edu.cn/ wheezy main non-free contrib
deb-src http://mirrors.bit6.edu.cn/ wheezy-proposed-updates main non-free contrib

	#清华大学（Ipv6）
deb http://mirrors.6.tuna.tsinghua.edu.cn/debian/ wheezy main non-free contrib
deb http://mirrors.6.tuna.tsinghua.edu.cn/ wheezy-proposed-updates main non-free contrib
deb-src http://mirrors.6.tuna.tsinghua.edu.cn/ wheezy main non-free contrib
deb-src http://mirrors.6.tuna.tsinghua.edu.cn/ wheezy-proposed-updates main non-free contrib
	```

- ***使用命令***
	- ***一种快捷的方式就是使用命令`add-apt-repository 源`,如下是添加kde桌面环境的镜像***
	***`add-apt-repository ppa:kubuntu-ppa/backports apt-get update`***

		>***还要一条一条的输干嘛还要说是快捷的方式呢,当由于某些原因不能方便的直接添加的时候,这个方式就显得很快捷了.***





---

### Vim常用命令

***vim命令集合***

- ***命令历史***
	- ***以`：`和`/`开头的命令都有历史纪录，可以通过先键入`：`或`/`然后按上下箭头来选择某个历史命令***
- ***启动`vim`***
	- ***在命令行键入vim直接启动`vim`***
	- ***键入`vim filename`打开vim并创建名为`filename`的文件***
- ***创建新文件***
	- ***`vi filename`***
- ***文件命令***
	- ***打开某个文件`vim filename`如果不存在就新建了***
	- ***同时打开多个文件 `vim file1 file2...`***
	- ***在vim窗口打开一个新文件 ：`open file`***
	- ***在新窗口打开文件:`split file`***
	- ***切换到下一个文件 ：`bn`***
	- ***切换到上一个文件：`bp`***
	- ***查看当前打开的文件列表，当前正在编辑的会用【】括起来 ：`args`***
	- ***打开远程文件 ：`e ftp://192.168.10.76/abc.txt`***
- ***vim的模式***
	- ***正常模式 按`esc`或`crtl[`进入，左下角显示文件名或空***
	- ***插入模式：按i进入，左下角显示`-insert-`***
	- ***可视模式：（不知道如何进入）左下角显示`-visual-`***
- ***导航命令***

---

### <span id="banben">查看版本</span>
>***一般常用的常见的linux版本分为debian(ubuntu)系列和redhat(centos)系列***

- ***查看内核版本***

	- ***`uname -a`***
	- ***`cat /proc/version `***

	>***人家说uname -a命令也是调用/proc/version这个文件,所以说效果一样***

- ***查看发行版版本***

	- ***`more /etc/issue`***
	- ***`lsb_release -a`***
	- ***`cat /etc/redhat-release`***
	- ***`rpm -q redhat-release`***

	>***带有redhat字眼的一般都是用于查看基于redhat的发行版***



---

### Iptables
>***这玩意是linux上的常用防火墙.***

***简单命令***
***`Iptables(选项)(参数)`***

***示例***
>***开关某个端口.如80端口.两种方式任选一种如下:***

- ***1.使用vi修改 (路径为:/etc/sysconfig/iptables)***  
	- ***添加`-A INPUT -p tcp -m tcp --dport 80 -j ACCEPT`并保存退出.***  
	- ***然后使用`service iptables restart`命令重启.***
- ***2.使用命令修改***  
	- ***直接输入命令`iptables -A INPUT -p tcp --dport 111 -j ACCEPT`***  
	- ***好像需要保存一下`/etc/rc.d/init.d/iptables save`.***  
	- ***然后也需要重启一下防火墙.***  

>***那么关闭端口的命令就是`iptables -A INPUT -p tcp --dport 111 -j DROP`.***

--

>***其他:***  
***开关某项服务如iptables***  

>- ***临时:***  
***开启： `service iptables start`***  
***关闭： `service iptables stop`***  
- ***永久:***  
***开启： `chkconfig iptables on`***  
***关闭： `chkconfig iptables off`***  

>>***其余服务临时和永久开关同样适用.***  

>
***iptables简单命令含义***

>- ***`:INPUT ACCEPT [0:0]`***  
***# 该规则表示INPUT表默认策略是ACCEPT***

>- ***`:FORWARD ACCEPT [0:0]`***  
***# 该规则表示FORWARD表默认策略是ACCEPT***

>- ***`:OUTPUT ACCEPT [0:0]`***  
***# 该规则表示OUTPUT表默认策略是ACCEPT***

>- ***`-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT`***  
***# 意思是允许进入的数据包只能是刚刚我发出去的数据包的回应，ESTABLISHED：已建立的链接状态。RELATED：该数据包与本机发出的数据包有关。***

>- ***`-A INPUT -j REJECT --reject-with icmp-host-prohibited`  
`-A FORWARD -j REJECT --reject-with icmp-host-prohibited`***  
***# 这两条的意思是在INPUT表和FORWARD表中拒绝所有其他不符合上述任何一条规则的数据包。并且发送一条host prohibited的消息给被拒绝的主机。***

>- ***aa:开放指定的端口***  
	>- ***`iptables -A INPUT -s 127.0.0.1 -d 127.0.0.1 -j ACCEPT` #允许本地回环接口(即运行本机访问本机)***
	>- ***`iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT` #允许已建立的或相关连的通行***  
	>- ***`iptables -A OUTPUT -j ACCEPT` #允许所有本机向外的访问***  
	>- ***`iptables -A INPUT -p tcp --dport 22 -j ACCEPT` #允许访问22端口***  
	>- ***`iptables -A INPUT -p tcp --dport 80 -j ACCEPT` #允许访问80端口***  
	>- ***`iptables -A INPUT -p tcp --dport 21 -j ACCEPT` #允许ftp服务的21端口***  
	>- ***`iptables -A INPUT -p tcp --dport 20 -j ACCEPT` #允许FTP服务的20端口***  
	>- ***`iptables -A INPUT -j reject` #禁止其他未允许的规则访问***  
	>- ***`iptables -A FORWARD -j REJECT` #禁止其他未允许的规则访问***  
	>- ***aa来自: http://man.linuxde.net/iptables***


---

### RAM/SWAP浅析
>***当初就是搜索概念性的东西都让我废了好大劲.首先呢,他们两个分别是什么意思.***

- ***RAM***  
	- ***一直搜"linux中的ram是什么",忙活了半天也不知道是什么玩意,后来搜"ram是什么",是内存,我...***  
	- ***内存不是硬盘空间,可以认为是一种临时空间,可以随时读取数据,从内存中读取数据比从硬盘中读取熟读要快.***  
	- ***就是说电脑内存是4G啊8G啊那个内存,不是C盘D盘动不动就几百G的那个硬盘内存.***

- ***SWAP***  
	- ***概念:意思是交换分区,是硬盘空间(Disk)被划出来冒充内存(RAM)的,当内存使用完的时候可以使用swap,这样想着好像是扩大内存了,所以一般情况下可以增加性能,但是一直访问swap空间会使机器/服务器越来越慢.***  
	- ***swap是用来存放内存溢出来的数据,但是并不一定是内存用完了之后才来用swap的空间,在内存中较少使用的数据会被移动到swap中,所以swap空间被占满的时候并不一定是内存被耗光了,swap分区空间被占满也不会影响性能.***  
	- ***详见 [Linux公社](http://www.linuxidc.com/Linux/2014-05/102272.htm)***

---


### <span id="linux">每天一个linux命令</span>

- ***几个通用的命令参数.***
	- ***`--help`***

		>***显示该命令的帮助信息.***

	- ***`--version`***

		>***显示该命令的版本信息.***

- ***ls***

	>***结构`ls [选项] [目录名]`  
选项就是参数的意思吧,可以跟各种各样的参数,目录的话如果不写,就是列出当前目录的文件了.下面给出几个较常见的参数/选项,太多了记不住而且又用不到,记他干嘛啊.我一般都是`ll`.***

	- ***`-l`***

		>***`l`相当于`list`,指列出所有详细信息.包括文件的权限/所有者/文件大小等.***

	- ***`-a`***

		>***`-a`和`-all`意思和效果都是一样的,这里`a`就是`all`的简写,表示列出所有文件,包括以`.`开头的隐含文件.***
	- ***`-A`***

		>***其实也没什么必要,一般用`-a`就好了.效果跟`-a`的区别只有是`-A`不列出`.`(当前目录)和`..`(当前目录的父级目录).***

	- ***`-c`***

		>***根据最后修改时间排序.如果要在排序的同时显示最后更改时间,需要配合`-lt`.如果要显示最后更改时间的同时按照名称排序,只需配合`-l`即可.***

	- ***`-g`***

		>***类似`-l`,但是`-g`显示的时候不会列出目录的所有者.***

	- ***`-h`***

		>***`h`就是`human`的意思.意即按照人类容易理解的格式列出文件大小.好像是单位由字节变为K/M/G乃至更大的单位.***

	- ***`-o`***

		>***类似`-l`,指显示出组信息外的详细信息.***

	- ***`-S`***

		>***根据文件大小排序.***

	- ***`-t`***

		>***根据文件最后修改时间排序.***\

	- ***`-b`***

		>***根据文件版本排序.***

	- ***`-X`***

		>***根据文件扩展名排序.***

	- ***`--help`***

		>***显示此帮助信息并离开.***

	- ***`--version`***

		>***显示版本信息并离开.***

	- ***`-1`***

		>***每行只列出一个文件.***

	- ***`-u`***

		>***配合`-lt`显示访问时间并且以访问时间排序.***

	- ***`-U`***

		>***根据文件系统原有的次序排序.***


	- ***`-R`***

		>***表示递进,同时列出所有子目录.***

***高级玩法:***
>***我见的多了,西方哪个国家我没去过.***

- ***在`ls`中列出文件的绝对路径***
	- ***`ls|sed "s:pwd/:"`***
- ***列出当前目录下的所有文件(包括隐藏文件)的绝对路径且对目录不递归***
	- ***`find $PWD -maxdepth 1 | xargs ls -ld`***
- ***列出当前目录下的所有文件(包括隐藏文件)的绝对路径且对目录递归***
	- ***`find $PWD | xargs ls -ld`***

--

- ***cd***  
>***格式是`cd [目录名]`.***

	- ***进入指定目录如`/opt/soft`***
		- ***`cd /opt/soft`***

	- ***回到上级目录***
		- ***`cd ..`***

	- ***进入当前用户目录***
		- ***`cd`或`cd ~`***


	- ***返回进入到当前用户目录的目录***
		- ***`cd -`***


	- ***把上个命令的参数作为cd的参数使用***
		- ***`cd !$`***

--


- ***pwd***  

	>***格式是`pwd [选项]`,用来查看当前工作目录的完整路径.***

	- ***如果目录是链接时***
		- ***`pwd -P`显示实际路径,`pwd`显示连接路径***

--

- ***mkdir***  

	>***格式是`mkdir [选项] 目录`,以前还不知道有那么多骚气的玩法.***

	- ***简单创建一个空目录***
		- ***`mkdir test1`***


	- ***递归创建多个***
		- ***`mkdir -p test1/test2`***

	- ***给新创建的目录设置权限***
		- ***`mkdir -m 777 test3`***

	- ***创建目录时显示提示信息***
		- ***`mkdir -v test1`***

>***我见得多了,下面是一个递归创建的高级玩法,在创建子目录的时候还可以创建子目录的子目录.***

- ***创建目录结构***
		- ***`mkdir -vp scf/{lib/,bin/,doc/{info,product},logs/{info,product},service/deploy/{info,product}}`***

--
- ***rm***  

	>***格式是`rm [选项] 文件/目录`,不是用`-r`参数的情况下不能删除目录,删除链接的时候删除不了原文件.***

	- ***删除一个文件,如果是`root`用户则不会询问是否删除***
		- ***`rm 文件名`***

	- ***非`root`用户也可以不询问是否删除***
		- ***`rm -f 文件名`***

	- ***交互式删除***
		- ***`rm -i *.log`***

	- ***遍历删除***
		- ***`rm -r test`***

	- ***遍历且强制删除***
		- ***`rm -rf test`***

	- ***删除相同后缀文件:1.log 2.long 3.log ***
		- ***`rm [123].log`***

>***高级玩法之自定义回收站功能,不过我没试验成功,不知道是什么原因.***

- ***`myrm(){ D=/tmp/$(date +%Y%m%d%H%M%S); mkdir -p $D; mv "$@" $D && echo "moved to $D ok"; }`***

--
- ***rmdir***  

	>***格式是`rm [选项] 目录`,可从一个目录中删除一个或多个子目录项,且删除某目录时也必须具有对父目录的写权限.***

	- ***不能删除非空目录***
		- ***`rmdir doc`***

	- ***当子目录不删除后使父目录也成为空目录的话,则顺便删除父目录***
		- ***`rmdir -p test`***

--
- ***mv***  

	>***格式是`rm [选项] 源文件/目录 目标文件/目录`,根据第二个参数的不同来决定是重命名还是移动到一个新位置,例如第一个和第二个参数类型相同且第二个参数不是已经存在的文件或文件目录,则执行重命名操作,如果两个参数都为文件且第二个参数存在同名文件,则覆盖已经存在的文件内容.***
	>>***不能把目录移动到文件.***

	- ***重命名,bbb文件/文件夹已经存在,重命名aaa为bbb且覆盖bbb的内容/或者理解为重命名aaa为bbb且删除bbb***
		- ***`mv aaa bbb`***
		- ***`mv aaa bbb/ccc`***
		- ***`mv aaa/ccc bbb/ddd`***
		- ***`mv aaa/ bbb/`***

	- ***重命名,bbb文件/文件夹不存在,把aaa重命名为bbb***
		- ***`mv aaa bbb`***
		- ***`mv aaa/ bbb/`***

	- ***移动,bbb目录已存在,把aaa目录移动到bbb目录下***
		- ***`mv aaa/ bbb/`***

	- ***移动,把aaa目录下的ccc移动到bbb目录下***
		- ***`mv aaa/ccc bbb/`***

>- ***这里是按照操作方式进行分类的(重命名/移动),也可以通过参数是否已经存在进行分类.***
- ***更多高级玩法可通过搜索引擎查询.暂有以下几例:***
	- ***命令参数***
		- ***`-b`:如果覆盖文件,则覆盖前先进行备份***
		- ***`-f`:如果目标文件已存在,直接强制覆盖***
		- ***`-i`:如果目标文件已存在,询问是否覆盖***


--

